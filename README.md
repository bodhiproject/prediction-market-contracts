# Bodhi Prediction Market on Nakachain

[![pipeline status](https://gitlab.com/bodhiproject/prediction-market-contracts/badges/master/pipeline.svg)](https://gitlab.com/bodhiproject/prediction-market-contracts/commits/master)
[![coverage report](https://gitlab.com/bodhiproject/prediction-market-contracts/badges/master/coverage.svg)](https://gitlab.com/bodhiproject/prediction-market-contracts/commits/master)

## Auto Tagging

Gitlab CI/CD will automatically create a new `tag` when a branch is merged to `master`. **Ensure** that the `package.json version` field is updated for any branches that will merge into `master`.
