pragma solidity ^0.5.10;

/// @title Event interface.
contract IEvent {
    uint8 internal constant DEFAULT_RESULT_INDEX = 255;

    // Storage
    uint8 internal _numOfResults;
    uint8 internal _currentRound = 0;
    uint8 internal _currentResultIndex = DEFAULT_RESULT_INDEX;
    address internal _bodhiTokenAddress;
    address internal _eventFactoryAddress;
    address internal _centralizedOracle;
    uint internal _betStartTime;
    uint internal _betEndTime;
    uint internal _resultSetStartTime;
    uint internal _resultSetEndTime;
    uint internal _escrowAmount;
    uint internal _arbitrationLength;
    uint internal _thresholdPercentIncrease;
    uint internal _arbitrationRewardPercentage;
    uint internal _totalBets;
    string internal _eventName;
    mapping(address => bool) internal _didWithdraw;

    // Events
    event EscrowDeposited(
        address indexed eventAddress,
        address indexed depositorAddress,
        uint depositAmount
    );
    event BetPlaced(
        address indexed eventAddress,
        address indexed better,
        uint8 resultIndex,
        uint amount,
        uint8 eventRound
    );
    event ResultSet(
        address indexed eventAddress,
        address indexed centralizedOracle,
        uint8 resultIndex,
        uint amount,
        uint8 eventRound,
        uint nextConsensusThreshold,
        uint nextArbitrationEndTime
    );
    event VotePlaced(
        address indexed eventAddress,
        address indexed voter,
        uint8 resultIndex,
        uint amount,
        uint8 eventRound
    );
    event VoteResultSet(
        address indexed eventAddress,
        address indexed voter,
        uint8 resultIndex,
        uint amount,
        uint8 eventRound,
        uint nextConsensusThreshold,
        uint nextArbitrationEndTime
    );
    event WinningsWithdrawn(
        address indexed eventAddress,
        address indexed winner,
        uint winningAmount,
        uint creatorReturn
    );

    // Modifiers
    modifier validResultIndex(uint8 resultIndex) {
        require (resultIndex <= _numOfResults - 1, "resultIndex is not valid");
        _;
    }

    // Functions
    function withdraw() external;
    function version() public pure returns (uint16);
    function currentRound() public view returns (uint8);
    function currentResultIndex() public view returns (uint8);
    function currentConsensusThreshold() public view returns (uint);
    function currentArbitrationEndTime() public view returns (uint);
    function centralizedMetadata() public view returns (address, uint, uint, uint, uint);
    function configMetadata() public view returns (uint, uint, uint, uint);
    function totalBets() public view returns (uint);
    function didWithdraw(address withdrawer) public view returns (bool);
    function bet(address from, uint8 resultIndex, uint value) private;
    function setResult(address from, uint8 resultIndex, uint value) private;
    function vote(address from, uint8 resultIndex, uint value) private;
    function voteSetResult(address from, uint8 resultIndex, uint value, uint refund) private;
    function finalizeResult() private;
}
