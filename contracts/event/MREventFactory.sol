pragma solidity ^0.5.10;

import "./MREvent.sol";
import "../storage/IConfigManager.sol";
import "../token/INRC223.sol";
import "../token/NRC223Receiver.sol";

/// @title MREventFactory allows the creation of Multiple Result Events.
contract MREventFactory is NRC223Receiver {
    using ByteUtils for bytes;
    using ByteUtils for bytes32;

    uint16 private constant VERSION = 7;
    bytes32 private constant BODHI_TOKEN_KEY = "bodhitoken";

    address private _configManager;
    address private _bodhiTokenAddress;

    // Events
    event MultipleResultsEventCreated(
        address indexed eventAddress,
        address indexed ownerAddress
    );

    /// @dev Creates a new MREventFactory.
    /// @param configManager ConfigManager address.
    constructor(address configManager) public {
        require(configManager != address(0), "configManager address is invalid");

        _configManager = configManager;
        _bodhiTokenAddress = IConfigManager(_configManager)
            .getAddress(BODHI_TOKEN_KEY);
        assert(_bodhiTokenAddress != address(0));
    }
    
    /// @dev Handle incoming token transfers needed for creating events.
    /// @param from Token sender address.
    /// @param value Amount of tokens.
    /// @param data The message data. First 4 bytes is function hash & rest is function params.
    function tokenFallback(
        address from,
        uint value,
        bytes calldata data)
        external
    {
        require(msg.sender == _bodhiTokenAddress, "Only NBOT is accepted");
        require(data.length >= 4, "Data is not long enough.");

        bytes memory funcHash = data.sliceBytes(0, 4);
        bytes memory params = data.sliceBytes(4, data.length - 4);
        bytes32 encodedFunc = keccak256(abi.encodePacked(funcHash));
        if (encodedFunc == keccak256(abi.encodePacked(hex"67d0df4c"))) {
            parseNewMREvent(from, value, params);
        } else {
            revert("Unhandled function in tokenFallback");
        }
    }

    function version() external pure returns (uint16) {
        return VERSION;
    }

    /// @dev Parses new MREvent params. Only tokenFallback can call this.
    /// @param from Token sender address.
    /// @param value Amount of tokens deposited to create the event.
    /// @param params The message data. First 4 bytes is function hash & rest is function params.
    function parseNewMREvent(
        address from,
        uint value,
        bytes memory params)
        private
    {
        (string memory eventName, bytes32[3] memory eventResults,
            uint betEndTime, uint resultSetStartTime, address centralizedOracle,
            uint8 arbitrationOptionIndex, uint arbitrationRewardPercentage) =
            abi.decode(params, (string, bytes32[3], uint, uint, address, uint8,
            uint));

        // Add Invalid option
        bytes32[4] memory results;
        uint8 numOfResults;
        (results, numOfResults) = insertInvalidResult(eventResults);

        newMREvent(from, eventName, results, numOfResults, value, betEndTime,
            resultSetStartTime, centralizedOracle, arbitrationOptionIndex,
            arbitrationRewardPercentage);
    }
    
    /// @dev Creates a new MREvent. Only parseNewMREvent can call this.
    function newMREvent(
        address from,
        string memory eventName,
        bytes32[4] memory results,
        uint8 numOfResults,
        uint escrowAmount,
        uint betEndTime,
        uint resultSetStartTime,
        address centralizedOracle,
        uint8 arbitrationOptionIndex,
        uint arbitrationRewardPercentage)
        private
    {   
        // Validate escrow amount
        uint minEscrowAmount = IConfigManager(_configManager).eventEscrowAmount();
        require(escrowAmount >= minEscrowAmount, "Escrow deposit is not enough");

        // Create event
         MREvent newEvent = new MREvent(
            from, eventName, results, numOfResults, escrowAmount, betEndTime,
            resultSetStartTime, centralizedOracle, arbitrationOptionIndex,
            arbitrationRewardPercentage, _configManager);

        // Transfer escrow to event
        INRC223(_bodhiTokenAddress).transfer(address(newEvent), escrowAmount);

        emit MultipleResultsEventCreated(address(newEvent), from);
    }

    /// @dev Inserts the Invalid option at the first array index.
    /// @param eventResults Event results array of names.
    /// @return Updated results array and number of non-empty results.
    function insertInvalidResult(
        bytes32[3] memory eventResults)
        private
        pure
        returns (bytes32[4] memory, uint8)
    {
        // Add Invalid result to eventResults
        bytes32[4] memory results;
        results[0] = "Invalid";

        // Copy results to new array with Invalid option.
        // Increament numOfResults if array item is not empty.
        uint8 numOfResults = 1;
        for (uint i = 0; i < eventResults.length; i++) {
            if (!eventResults[i].isEmpty()) {
                results[i + 1] = eventResults[i];
                numOfResults++;
            } else {
                break;
            }
        }

        return (results, numOfResults);
    }
}
