pragma solidity ^0.5.10;

import "./ABEvent.sol";
import "../storage/IConfigManager.sol";
import "../token/INRC223.sol";
import "../token/NRC223Receiver.sol";
import "../lib/SafeMath.sol";

/// @title ABEventFactory allows the creation of AB Events.
contract ABEventFactory is NRC223Receiver {
    using ByteUtils for bytes;
    using ByteUtils for bytes32;
    using SafeMath for uint;

    uint16 private constant VERSION = 1;
    bytes32 private constant BODHI_TOKEN_KEY = "bodhitoken";

    address private _configManager;
    address private _bodhiTokenAddress;

    // Events
    event ABEventCreated(
        address indexed eventAddress,
        address indexed ownerAddress
    );

    /// @dev Creates a new ABEventFactory.
    /// @param configManager ConfigManager address.
    constructor(address configManager) public {
        require(configManager != address(0), "configManager address is invalid");

        _configManager = configManager;
        _bodhiTokenAddress = IConfigManager(_configManager)
            .getAddress(BODHI_TOKEN_KEY);
        require(_bodhiTokenAddress != address(0));
    }
    
    /// @dev Handle incoming token transfers needed for creating events.
    /// @param from Token sender address.
    /// @param value Amount of tokens.
    /// @param data The message data. First 4 bytes is function hash & rest is function params.
    function tokenFallback(
        address from,
        uint value,
        bytes calldata data)
        external
    {
        require(msg.sender == _bodhiTokenAddress, "Only NBOT is accepted");
        require(data.length >= 4, "Data is not long enough.");

        bytes memory funcHash = data.sliceBytes(0, 4);
        bytes memory params = data.sliceBytes(4, data.length - 4);
        bytes32 encodedFunc = keccak256(abi.encodePacked(funcHash));
        if (encodedFunc == keccak256(abi.encodePacked(hex"5280f033"))) {
            newABEvent(from, value, params);
        } else {
            revert("Unhandled function in tokenFallback");
        }
    }

    function version() external pure returns (uint16) {
        return VERSION;
    }

    /// @dev Creates a new ABEvent. Only tokenFallback can call this.
    /// @param from Token sender address.
    /// @param value Amount of tokens deposited to create the event.
    /// @param params The message data. First 4 bytes is function hash & rest is function params.
    function newABEvent(
        address from,
        uint value,
        bytes memory params)
        private
    {   
        // Validate escrow amount
        uint minEscrowAmount = IConfigManager(_configManager).eventEscrowAmount();
        require(value >= minEscrowAmount, "Escrow deposit is not enough");

        (string memory eventName, bytes32[2] memory eventResults,
            uint betEndTime, uint resultSetStartTime, address centralizedOracle,
            uint8 arbitrationOptionIndex, uint arbitrationRewardPercentage,
            uint8[2] memory winningChances) =
            abi.decode(params, (string, bytes32[2], uint, uint, address, uint8,
            uint, uint8[2]));

        // Create event
        ABEvent newEvent = new ABEvent(
            from, eventName, insertInvalidResult(eventResults),
            value, betEndTime, resultSetStartTime, 
            centralizedOracle, arbitrationOptionIndex,
            arbitrationRewardPercentage, _configManager,
            insertInvalidChance(winningChances));

        // Transfer escrow to event
        INRC223(_bodhiTokenAddress).transfer(address(newEvent), value);

        emit ABEventCreated(address(newEvent), from);
    }

    function insertInvalidResult(
        bytes32[2] memory eventResults)
        private
        pure
        returns (bytes32[3] memory)
    {
        // Add Invalid result to eventResults
        bytes32[3] memory results;
        results[0] = "Invalid";

        // Copy results to new array with Invalid option
        for (uint i = 0; i < 2; i++) {
            results[i + 1] = eventResults[i];
        }

        return results;
    }

    function insertInvalidChance(
        uint8[2] memory winningChances)
        private
        pure
        returns (uint8[3] memory)
    {
        // Add 0% chance for Invalid option
        uint8[3] memory chances;
        chances[0] = 0;

        // Copy results to new array with Invalid option
        for (uint i = 0; i < 2; i++) {
            chances[i + 1] = winningChances[i];
        }

        return chances;
    }
}
