pragma solidity ^0.5.10;

import "./IEvent.sol";
import "../storage/IConfigManager.sol";
import "../token/INRC223.sol";
import "../token/NRC223Receiver.sol";
import "../lib/Ownable.sol";
import "../lib/SafeMath.sol";
import "../lib/ByteUtils.sol";

/// @title A or B Event
contract ABEvent is NRC223Receiver, Ownable, IEvent {
    using ByteUtils for bytes;
    using ByteUtils for bytes32;
    using SafeMath for uint;

    struct EventRound {
        bool finished;
        uint8 lastResultIndex;
        uint8 resultIndex;
        address resultSetter;
        uint consensusThreshold;
        uint arbitrationEndTime;
    }

    uint16 private constant VERSION = 1;
    bytes32 private constant BODHI_TOKEN_KEY = "bodhitoken";
    bytes32 private constant AB_FACTORY_KEY = "abfactory";
    uint256 private constant ORACLE_RESULT_SETTING_LENGTH = 48 * 60 * 60; // 48 hours

    uint8[3] private _winningChances;
    bytes32[3] private _eventResults;
    uint[3] private _betRoundTotals;
    uint[3] private _voteRoundsTotals;
    uint[3] private _currentVotingRoundTotals;
    uint[3] private _odds;
    mapping(address => uint[3]) private _betRoundUserTotals;
    mapping(address => uint[3]) private _voteRoundsUserTotals;
    mapping(uint8 => EventRound) private _eventRounds;

    /// @notice Creates a new StandardEvent contract.
    /// @param owner Address of the owner.
    /// @param eventName Question or statement prediction.
    /// @param eventResults Possible results.
    /// @param escrowAmount Amount of escrow deposited when creating the event.
    /// @param betEndTime Unix time when betting will end.
    /// @param resultSetStartTime Unix time when the CentralizedOracle can set the result.
    /// @param centralizedOracle Address of the user that will decide the result.
    /// @param arbitrationOptionIndex Index of the selected arbitration option.
    /// @param arbitrationRewardPercentage Percentage of loser's bets going to winning arbitrators.
    /// @param configManager Address of the ConfigManager.
    constructor(
        address owner,
        string memory eventName,
        bytes32[3] memory eventResults,
        uint escrowAmount,
        uint betEndTime,
        uint resultSetStartTime,
        address centralizedOracle,
        uint8 arbitrationOptionIndex,
        uint arbitrationRewardPercentage,
        address configManager,
        uint8[3] memory winningChances)
        Ownable(owner)
        public
        validAddress(centralizedOracle)
        validAddress(configManager)
    {
        bytes memory eventNameBytes = bytes(eventName);
        require(eventNameBytes.length > 0, "Event name cannot be empty");
        require(
            !eventResults[1].isEmpty(),
            "First event result cannot be empty");
        require(
            !eventResults[2].isEmpty(),
            "Second event result cannot be empty");
        require(
            betEndTime > block.timestamp,
            "betEndTime should be > current time");
        require(
            resultSetStartTime >= betEndTime,
            "resultSetStartTime should be >= betEndTime");
        require(
            arbitrationOptionIndex < 4,
            "arbitrationOptionIndex should be < 4");
        require(
            arbitrationRewardPercentage < 100,
            "arbitrationRewardPercentage should be < 100");
        require(
            winningChances[1] < 100,
            "winningChance 1 should be < 100");
        require(
            winningChances[2] < 100,
            "winningChance 2 should be < 100");
        require(
            winningChances[1] + winningChances[2] == 100,
            "winningChance 1 add winningChance 2 should be equal to 100");

        _eventName = eventName;
        _eventResults = eventResults;
        _numOfResults = 3;
        _escrowAmount = escrowAmount;
        _betStartTime = block.timestamp;
        _betEndTime = betEndTime;
        _resultSetStartTime = resultSetStartTime;
        _resultSetEndTime = resultSetStartTime.add(ORACLE_RESULT_SETTING_LENGTH);
        _centralizedOracle = centralizedOracle;
        _arbitrationRewardPercentage = arbitrationRewardPercentage;
        _winningChances = winningChances;

        // Calculate odds
        uint winningTotal = 100 * 100;
        _odds[0] = 0;
        _odds[1] = winningTotal
            .sub(_arbitrationRewardPercentage.mul(100))
            .div(uint256(_winningChances[1]));
        _odds[2] = winningTotal
            .sub(_arbitrationRewardPercentage.mul(100))
            .div(uint256(_winningChances[2]));

        // Fetch current config
        IConfigManager config = IConfigManager(configManager);
        _bodhiTokenAddress = config.getAddress(BODHI_TOKEN_KEY);
        require(_bodhiTokenAddress != address(0));
        _eventFactoryAddress = config.getAddress(AB_FACTORY_KEY);
        require(_eventFactoryAddress != address(0));
        _arbitrationLength = config.arbitrationLength()[arbitrationOptionIndex];
        _thresholdPercentIncrease = config.thresholdPercentIncrease();

        // Init CentralizedOracle round
        initEventRound(
            0,
            DEFAULT_RESULT_INDEX,
            config.startingConsensusThreshold()[arbitrationOptionIndex],
            0
        );

        emit EscrowDeposited(address(this), owner, escrowAmount);
    }

    /// @dev Standard NRC223 function that will handle incoming token transfers.
    /// @param from Token sender address.
    /// @param value Amount of tokens.
    /// @param data The message data. First 4 bytes is function hash & rest is function params.
    function tokenFallback(
        address from,
        uint value,
        bytes calldata data)
        external
    {
        require(msg.sender == _bodhiTokenAddress, "Only NBOT is accepted");
        require(data.length >= 4, "Data is not long enough");

        bytes memory funcHash = data.sliceBytes(0, 4);
        bytes32 funcCalled = keccak256(abi.encodePacked(funcHash));
        if (funcCalled == keccak256(abi.encodePacked(hex"47e7ef24"))) {
            deposit(from, value);
            return;
        }

        bytes memory params = data.sliceBytes(4, data.length - 4);
        (uint8 resultIndex) = abi.decode(params, (uint8));
        if (funcCalled == keccak256(abi.encodePacked(hex"885ab66d"))) {
            bet(from, resultIndex, value);
        } else if (funcCalled == keccak256(abi.encodePacked(hex"a6b4218b"))) {
            setResult(from, resultIndex, value);
        } else if (funcCalled == keccak256(abi.encodePacked(hex"1e00eb7f"))) {
            vote(from, resultIndex, value);
        } else {
            revert("Unhandled function in tokenFallback");
        }
    }

    /// @notice Withdraw winnings if the DecentralizedOracle round arbitrationEndTime has passed.
    function withdraw() external {
        require(_currentRound > 0, "Cannot withdraw during betting round.");
        require(
            block.timestamp >= _eventRounds[_currentRound].arbitrationEndTime,
            "Current time should be >= arbitrationEndTime");
        require(!_didWithdraw[msg.sender], "Already withdrawn");

        // Finalize the result if not already done
        if (!_eventRounds[_currentRound].finished) {
            finalizeResult();
        }

        // Calculate and transfer winnings
        _didWithdraw[msg.sender] = true;
        uint winningAmount = calculateWinnings(msg.sender);

        // Transfer escrow if owner
        uint escrowAmount = 0;
        uint unpaidBet = 0;
        if (msg.sender == owner) {
            (escrowAmount, unpaidBet) = calculateEscrowAndUnpaidBet();
        }

        // Transfer tokens
        uint total = winningAmount.add(escrowAmount).add(unpaidBet);
        if (total > 0) {
            INRC223(_bodhiTokenAddress).transfer(msg.sender, total);
        }
        
        // Emit events
        uint creatorRewardAmt;
        uint resultSetterRewardAmt;
        (creatorRewardAmt, resultSetterRewardAmt) = calculateArbRewards(msg.sender);
        emit WinningsWithdrawn(address(this), msg.sender, winningAmount, 
            escrowAmount.add(unpaidBet));
    }

    /// @notice Returns the maximum bet allowed for each resultIndex.
    /// @return Array of allowable max bets.
    function maxBets() public view returns (uint, uint, uint) {
        uint maxBetA = getMaxBetAmount(1);
        uint maxBetB = getMaxBetAmount(2);

        return (
            0,
            maxBetA,
            maxBetB
        );
    }

    /// @notice Calculates the tokens returned based on the sender's participation.
    /// @param player Address to calculate winnings for.
    /// @return Amount of tokens that will be returned.
    function calculateWinnings(
        address player)
        public
        view
        returns (uint)
    {
        // Return 0 if there currentResultIndex is default since we cannot
        // iterate through the balances by that index.
        if (_currentResultIndex == DEFAULT_RESULT_INDEX) {
            return (0);
        }

        // Bets are returned if the currentResultIndex is Invalid
        uint betRoundWinningAmt;
        uint voteRoundsWinningAmt;
        uint creatorRewardAmt;
        uint resultSetterRewardAmt;
        if (_currentResultIndex == 0) {
            (
                betRoundWinningAmt,
                voteRoundsWinningAmt
            ) = calculateInvalidWinnings(player);
        } else {
            (
                betRoundWinningAmt,
                voteRoundsWinningAmt
            ) = calculateNormalWinnings(player);
        }

        (
            creatorRewardAmt,
            resultSetterRewardAmt
        ) = calculateArbRewards(player);

        return betRoundWinningAmt
            .add(voteRoundsWinningAmt)
            .add(creatorRewardAmt)
            .add(resultSetterRewardAmt);
    }

    /// @dev Calculate amount of escrow left and unpaid bet amount.
    /// @return Amount of escrow left and unpaid bet.
    function calculateEscrowAndUnpaidBet() public view returns (uint, uint) {
        // Invalid result 0 will return all the bets back.
        // Also, need to return 0 on DEFAULT_RESULT_INDEX since we cannot
        // iterate through balances by that index.
        if (_currentResultIndex == 0
            || _currentResultIndex == DEFAULT_RESULT_INDEX) {
                return (_escrowAmount, 0);
        }
        uint maxPercent = 100;
        uint betRoundWinningAmt = _betRoundTotals[_currentResultIndex]
            .mul(_odds[_currentResultIndex])
            .div(maxPercent); // Divide by 100 since odds are stored as whole percentage, e.g. 120% = 120
        uint betRoundPool;
        uint losingBets;
        for (uint i = 1; i < _numOfResults; i++) {
            betRoundPool = betRoundPool.add(_betRoundTotals[i]);
            if (i != _currentResultIndex) {
                losingBets = _betRoundTotals[i];
            }
        }

        // exclude arbReward from pool
        betRoundPool = betRoundPool
            .sub(losingBets.mul(_arbitrationRewardPercentage).div(maxPercent));
        uint unpaidBet;
        uint remainingEscrow = _escrowAmount;
        if (betRoundPool < betRoundWinningAmt) {
            // unpaidBet is not enough to payout
            uint escrowUsed = betRoundWinningAmt.sub(betRoundPool);
            if (losingBets == 0) {
                // substract the losing escrow
                // 90% * losing_escrow = betRoundWinningAmt - betPool
                // losing_escrow = (betRoundWinningAmt - betPool) / 90 * 100
                escrowUsed = betRoundWinningAmt
                    .sub(_betRoundTotals[_currentResultIndex])
                    .mul(maxPercent)
                    .div(maxPercent.sub(_arbitrationRewardPercentage));
            }
            remainingEscrow = remainingEscrow
                .sub(escrowUsed);
        } else {
            unpaidBet = betRoundPool.sub(betRoundWinningAmt);
        }

        return (
            remainingEscrow,
            unpaidBet
        );
    }

    /// @notice Calculates the returns of the sender.
    /// @param player Address to get the withdraw amounts for.
    /// @return Array of amounts that player will receive.
    function getWithdrawAmounts(
        address player)
        public
        view
        returns (uint, uint, uint)
    {
        // Return 0 if the currentResultIndex is default since we cannot
        // iterate through the balances by that index.
        if (_currentResultIndex == DEFAULT_RESULT_INDEX) {
            return (0, 0, 0);
        }

        uint betRoundWinningAmt;
        uint voteRoundsWinningAmt;
        uint creatorRewardAmt;
        uint resultSetterRewardAmt;
        if (_currentResultIndex == 0) {
            (
                betRoundWinningAmt,
                voteRoundsWinningAmt
            ) = calculateInvalidWinnings(player);
        } else {
            (
                betRoundWinningAmt,
                voteRoundsWinningAmt
            ) = calculateNormalWinnings(player);
        }

        (
            creatorRewardAmt,
            resultSetterRewardAmt
        ) = calculateArbRewards(player);

        uint remainingEscrow = 0;
        uint unpaidBet = 0;
        if (player == owner) {
            (remainingEscrow, unpaidBet) = calculateEscrowAndUnpaidBet();
        }

        return (
            betRoundWinningAmt, // player's return
            voteRoundsWinningAmt.add(resultSetterRewardAmt), // voter's return
            remainingEscrow.add(unpaidBet).add(creatorRewardAmt) // creator's return
        );
    }

    function version() public pure returns (uint16) {
        return VERSION;
    }

    function currentRound() public view returns (uint8) {
        return _currentRound;
    }

    function currentResultIndex() public view returns (uint8) {
        return _currentResultIndex;
    }

    function currentConsensusThreshold() public view returns (uint) {
        return _eventRounds[_currentRound].consensusThreshold;
    }

    function currentArbitrationEndTime() public view returns (uint) {
        return _eventRounds[_currentRound].arbitrationEndTime;
    }

    function eventMetadata()
        public
        view
        returns (uint16, string memory, bytes32[3] memory,
        uint8, uint8[3] memory, uint[3] memory)
    {
        return (
            VERSION,
            _eventName,
            _eventResults,
            _numOfResults,
            _winningChances,
            _odds
        );
    }

    function centralizedMetadata()
        public
        view
        returns (address, uint, uint, uint, uint)
    {
        return (
            _centralizedOracle,
            _betStartTime,
            _betEndTime,
            _resultSetStartTime,
            _resultSetEndTime
        );
    }

    function configMetadata() public view returns (uint, uint, uint, uint) {
        return (
            _escrowAmount,
            _arbitrationLength,
            _thresholdPercentIncrease,
            _arbitrationRewardPercentage
        );
    }

    function totalBets() public view returns (uint) {
        return _totalBets;
    }

    function didWithdraw(address withdrawer) public view returns (bool) {
        return _didWithdraw[withdrawer];
    }

    function initEventRound(
        uint8 roundIndex,
        uint8 lastResultIndex,
        uint consensusThreshold,
        uint arbitrationEndTime)
        private
    {
        _eventRounds[roundIndex].lastResultIndex = lastResultIndex;
        _eventRounds[roundIndex].resultIndex = DEFAULT_RESULT_INDEX;
        _eventRounds[roundIndex].consensusThreshold = consensusThreshold;
        _eventRounds[roundIndex].arbitrationEndTime = arbitrationEndTime;
    }

    /// @dev Deposit more escrow
    /// @param from Address who is depositing.
    /// @param value Amount of tokens used to deposit.
    function deposit(
        address from,
        uint value)
        private
    {
        require(from == owner, "Only owner can deposit");
        require(_currentRound == 0, "Cannot deposit after betting round");
        require(value > 0, "Escrow deposit is not larger than 0");
        require(
            block.timestamp < _betEndTime,
            "Current time should be < betEndTime");

        _escrowAmount = _escrowAmount.add(value);
        emit EscrowDeposited(address(this), from, value);
    }

    /// @notice Places a bet. Only tokenFallback should call this.
    /// @param from Address who is betting.
    /// @param resultIndex Index of the result to bet on.
    /// @param value Amount of tokens used to bet.
    function bet(
        address from,
        uint8 resultIndex,
        uint value)
        private
        validResultIndex(resultIndex)
    {
        require(_currentRound == 0, "Can only bet during the betting round");
        require(
            block.timestamp >= _betStartTime,
            "Current time should be >= betStartTime");
        require(
            block.timestamp < _betEndTime,
            "Current time should be < betEndTime.");
        require(value > 0, "Bet amount should be > 0");
        require(getMaxBetAmount(resultIndex) >= value, "bet amount exceeded");

        // Update balances
        _totalBets = _totalBets.add(value);
        _betRoundTotals[resultIndex] = _betRoundTotals[resultIndex].add(value);
        _betRoundUserTotals[from][resultIndex] =
            _betRoundUserTotals[from][resultIndex].add(value);

        // Emit events
        emit BetPlaced(address(this), from, resultIndex, value, _currentRound);
    }

    /// @dev Centralized Oracle sets the result. Only tokenFallback should be calling this.
    /// @param from Address who is setting the result.
    /// @param resultIndex Index of the result to set.
    /// @param value Amount of tokens that was sent when calling setResult.
    function setResult(
        address from,
        uint8 resultIndex,
        uint value)
        private
        validResultIndex(resultIndex)
    {
        require(_currentRound == 0, "Can only set result during the betting round");
        require(!_eventRounds[0].finished, "Result has already been set");
        require(
            block.timestamp >= _resultSetStartTime,
            "Current time should be >= resultSetStartTime");
        if (block.timestamp < _resultSetEndTime) {
            require(
                from == _centralizedOracle,
                "Only the Centralized Oracle can set the result");
        }
        require(
            value == _eventRounds[0].consensusThreshold,
            "Set result amount should = consensusThreshold");

        // Update status and result
        _eventRounds[0].finished = true;
        _eventRounds[0].resultIndex = resultIndex;
        _eventRounds[0].resultSetter = from;
        _currentResultIndex = resultIndex;
        _currentRound = _currentRound + 1;

        // Update balances
        _totalBets = _totalBets.add(value);
        _voteRoundsTotals[resultIndex] = _voteRoundsTotals[resultIndex].add(value);
        _voteRoundsUserTotals[from][resultIndex] =
            _voteRoundsUserTotals[from][resultIndex].add(value);

        // Init DecentralizedOracle round
        uint nextThreshold = getNextThreshold(_eventRounds[0].consensusThreshold);
        uint arbitrationEndTime = block.timestamp.add(_arbitrationLength);
        initEventRound(
            _currentRound,
            resultIndex,
            nextThreshold,
            arbitrationEndTime);

        // Emit events
        emit ResultSet(address(this), from, resultIndex, value, 0,
            nextThreshold, arbitrationEndTime);
    }

    /// @dev Vote against the current result. Only tokenFallback should be calling this.
    /// @param from Address who is voting.
    /// @param resultIndex Index of result to vote.
    /// @param value Amount of tokens used to vote.
    function vote(
        address from,
        uint8 resultIndex,
        uint value)
        private
        validResultIndex(resultIndex)
    {
        require(_currentRound > 0, "Can only vote after the betting round");
        require(
            block.timestamp < _eventRounds[_currentRound].arbitrationEndTime,
            "Current time should be < arbitrationEndTime");
        require(
            resultIndex != _eventRounds[_currentRound].lastResultIndex,
            "Cannot vote on the last result index");
        require(value > 0, "Vote amount should be > 0");

        // Calculate diff if over threshold
        uint adjustedValue = value;
        uint refund;
        if (_currentVotingRoundTotals[resultIndex].add(value) >
            _eventRounds[_currentRound].consensusThreshold) {
            adjustedValue = _eventRounds[_currentRound].consensusThreshold
                .sub(_currentVotingRoundTotals[resultIndex]);
            refund = _currentVotingRoundTotals[resultIndex]
                .add(value)
                .sub(_eventRounds[_currentRound].consensusThreshold);
        }

        // Update balances
        _totalBets = _totalBets.add(adjustedValue);
        _voteRoundsTotals[resultIndex] =
            _voteRoundsTotals[resultIndex].add(adjustedValue);
        _voteRoundsUserTotals[from][resultIndex] =
            _voteRoundsUserTotals[from][resultIndex].add(adjustedValue);
        _currentVotingRoundTotals[resultIndex] = 
            _currentVotingRoundTotals[resultIndex].add(adjustedValue);

        // Emit events
        emit VotePlaced(address(this), from, resultIndex, adjustedValue,
            _currentRound);

        // If voted over the threshold, create a new DecentralizedOracle round
        if (_currentVotingRoundTotals[resultIndex] ==
            _eventRounds[_currentRound].consensusThreshold) {
            voteSetResult(from, resultIndex, adjustedValue, refund);
        }
    }

    /// @dev Result got voted over the threshold so start a new DecentralizedOracle round.
    /// @param from Address who is voted over the threshold.
    /// @param resultIndex Index of result that was voted over the threshold.
    /// @param value Amount of tokens used to vote.
    /// @param refund Amount to refund for voting over the threshold.
    function voteSetResult(
        address from,
        uint8 resultIndex,
        uint value,
        uint refund)
        private
    {
        // Calculate next consensus threshold
        uint currThreshold = _eventRounds[_currentRound].consensusThreshold;
        uint nextThreshold =
            getNextThreshold(_eventRounds[_currentRound].consensusThreshold);
        uint8 previousRound = _currentRound;

        // Update status and result
        _eventRounds[_currentRound].resultIndex = resultIndex;
        _eventRounds[_currentRound].finished = true;
        _currentResultIndex = resultIndex;
        _currentRound = _currentRound + 1;

        // Clear current voting round totals
        delete _currentVotingRoundTotals;

        // Init next DecentralizedOracle round
        uint arbitrationEndTime = block.timestamp.add(_arbitrationLength);
        initEventRound(
            _currentRound,
            resultIndex,
            nextThreshold,
            arbitrationEndTime);

        // Refund difference over threshold
        if (refund > 0) {
            INRC223(_bodhiTokenAddress).transfer(from, refund);
        }

        // Emit events
        emit VoteResultSet(address(this), from, resultIndex, currThreshold, 
            previousRound, nextThreshold, arbitrationEndTime);
    }

    /// @dev Finalizes the result before doing a withdraw.
    function finalizeResult() private {
        _eventRounds[_currentRound].finished = true;
    }

    function getNextThreshold(
        uint currentThreshold)
        private
        view
        returns (uint)
    {
        uint increment = _thresholdPercentIncrease.mul(currentThreshold).div(100);
        return currentThreshold.add(increment);
    }

    function getMaxBetAmount(
        uint8 resultIndex)
        private
        view
        returns (uint)
    {
        if (resultIndex == 0) return 0;
        uint maxBet;
        uint maxPercent = 100;
        if (_odds[resultIndex] > maxPercent) {
            uint remainingPercent = maxPercent.sub(_arbitrationRewardPercentage);
            // maxA * oddA = (escrow + betsB) * (1-profitCut%) + maxA
            // (oddA - 1) * maxA = (escrow + betsB) * (1-profitCut%)
            // maxA = (escrow + betsB) * (1-profitCut%) / (oddA - 1)
            // maxA = maxBetOnA + oldBetsA
            // maxBetOnA = (escrow + betsB) * (1-profitCut%) / (oddA - 1) - oldBetsA
            maxBet = _escrowAmount
                .add(_betRoundTotals[3 - resultIndex])
                .mul(remainingPercent)
                .div((_odds[resultIndex].sub(maxPercent)))
                .sub(_betRoundTotals[resultIndex]);
        }

        return maxBet;
    }

    /// @notice Calculates creatorRewardAmt and resultSetterRewardAmt.
    /// @param player Address to calculate returns for.
    /// @return creatorRewardAmt and resultSetterRewardAmt.
    function calculateArbRewards(
        address player)
        private
        view
        returns (uint, uint)
    {
        // If result is invalid, no arbRewards
        if (_currentResultIndex == 0) {
            return (0, 0);
        }
        uint maxPercent = 100;
        // Calculate bet and vote rounds losing totals
        uint betRoundLosersTotal = _betRoundTotals[3 - _currentResultIndex];
        // When no one loses, the amount of escrow to pay the winner is the losing bet
        // 90% * losing_bet = betRoundWinningAmt - betPool
        // losing_bet = (betRoundWinningAmt - betPool) / 90 * 100
        if (betRoundLosersTotal == 0) {
            uint betRoundWinningAmt = _betRoundTotals[_currentResultIndex]
                .mul(_odds[_currentResultIndex])
                .div(maxPercent);
            if (_betRoundTotals[_currentResultIndex] < betRoundWinningAmt) {
                betRoundLosersTotal = betRoundWinningAmt
                    .sub(_betRoundTotals[_currentResultIndex])
                    .mul(maxPercent)
                    .div(maxPercent.sub(_arbitrationRewardPercentage));
            }
        }

        // Calculate amount won from arbitration reward.
        uint arbitrationReward =
            betRoundLosersTotal
            .mul(_arbitrationRewardPercentage)
            .div(maxPercent);
        uint creatorRewardAmt;
        if (player == owner) {
            creatorRewardAmt =
                arbitrationReward
                .mul(50)
                .div(maxPercent);
        }
        uint resultSetterRewardAmt;
        if (_voteRoundsUserTotals[player][_currentResultIndex] > 0) {
            // 50% * myWinningVotes / winningVotes
            resultSetterRewardAmt = arbitrationReward
                .mul(50)
                .div(maxPercent)
                .mul(_voteRoundsUserTotals[player][_currentResultIndex])
                .div(_voteRoundsTotals[_currentResultIndex]);
        }

        return (
            creatorRewardAmt,
            resultSetterRewardAmt
        );
    }

    /// @notice Calculates the tokens returned for a non-Invalid final result.
    /// @param player Address to calculate returns for.
    /// @return Array of amount of tokens that will be returned.
    function calculateNormalWinnings(
        address player)
        private
        view
        returns (uint, uint)
    {
        // Calculate bet and vote rounds losing totals
        uint voteRoundsLosersTotal;
        for (uint8 i = 0; i < _numOfResults; i++) {
            if (i != _currentResultIndex) {
                voteRoundsLosersTotal =
                    voteRoundsLosersTotal.add(_voteRoundsTotals[i]);
            }
        }

        // Calculate amount won for bet round.
        // Odds already subtracted the arbRewardPercentage.
        uint betRoundWinningAmt =
            _betRoundUserTotals[player][_currentResultIndex]
            .mul(_odds[_currentResultIndex])
            .div(100); // Divide by 100 since odds are stored as whole percentage, e.g. 120% = 120

        // Calculate amount won for all vote rounds.
        uint voteRoundsWinningAmt =
            _voteRoundsUserTotals[player][_currentResultIndex]
            .mul(voteRoundsLosersTotal)
            .div(_voteRoundsTotals[_currentResultIndex])
            .add(_voteRoundsUserTotals[player][_currentResultIndex]);

        return (
            betRoundWinningAmt,
            voteRoundsWinningAmt
        );
    }

    /// @notice Calculates the tokens returned for an Invalid final result.
    ///         All bets are returned if an Invalid result.
    /// @param player Address to calculate winnings for.
    /// @return Array of amount of tokens that will be returned.
    function calculateInvalidWinnings(
        address player)
        private
        view
        returns (uint, uint)
    {
        // Calculate user's winning bets
        uint myWinningBets = _betRoundUserTotals[player][_currentResultIndex];
        uint myWinningVotes = _voteRoundsUserTotals[player][_currentResultIndex];

        // Calculate user's losing bets and vote rounds losing totals
        uint myLosingBets;
        uint voteRoundsLosersTotal;
        for (uint8 i = 0; i < _numOfResults; i++) {
            if (i != _currentResultIndex) {
                myLosingBets = myLosingBets.add(_betRoundUserTotals[player][i]);
                voteRoundsLosersTotal =
                    voteRoundsLosersTotal.add(_voteRoundsTotals[i]);
            }
        }

        // Calculate user's winning amount for vote rounds
        uint voteRoundsWinningAmt;
        if (myWinningVotes > 0 && _voteRoundsTotals[_currentResultIndex] > 0) {
            voteRoundsWinningAmt =
                voteRoundsLosersTotal
                .mul(myWinningVotes)
                .div(_voteRoundsTotals[_currentResultIndex]);
        }

        return (
            myWinningBets.add(myLosingBets),
            myWinningVotes.add(voteRoundsWinningAmt)
        );
    }
}
