pragma solidity ^0.5.10;

import "./RewardEvent.sol";
import "../storage/IConfigManager.sol";
import "../token/INRC223.sol";
import "../token/NRC223Receiver.sol";
import "../lib/SafeMath.sol";

/// @title EventFactory allows the creation of individual prediction events.
contract RewardEventFactory is NRC223Receiver {
    using ByteUtils for bytes;
    using ByteUtils for bytes32;
    using SafeMath for uint;

    uint16 private constant VERSION = 1;
    bytes32 private constant BODHI_TOKEN_KEY = "bodhitoken";

    address private _configManager;
    address private _bodhiTokenAddress;

    // Events
    event RewardEventCreated(
        address indexed eventAddress,
        address indexed ownerAddress
    );

    /// @dev Creates a new EventFactory.
    /// @param configManager ConfigManager address.
    constructor(address configManager) public {
        require(configManager != address(0), "configManager address is invalid");

        _configManager = configManager;
        _bodhiTokenAddress = IConfigManager(_configManager)
            .getAddress(BODHI_TOKEN_KEY);
        require(_bodhiTokenAddress != address(0));
    }
    
    /// @dev Handle incoming token transfers needed for creating events.
    /// @param from Token sender address.
    /// @param value Amount of tokens.
    /// @param data The message data. First 4 bytes is function hash & rest is function params.
    function tokenFallback(
        address from,
        uint value,
        bytes calldata data)
        external
    {
        require(msg.sender == _bodhiTokenAddress, "Only NBOT is accepted");
        require(data.length >= 4, "Data is not long enough.");

        bytes memory funcHash = data.sliceBytes(0, 4);
        bytes memory params = data.sliceBytes(4, data.length - 4);
        bytes32 encodedFunc = keccak256(abi.encodePacked(funcHash));
        if (encodedFunc == keccak256(abi.encodePacked(hex"5280f033"))) {
            newRewardEvent(from, value, params);
        } else {
            revert("Unhandled function in tokenFallback");
        }
    }

    function version() external pure returns (uint16) {
        return VERSION;
    }

    /// @dev Creates a new RewardEvent. Only tokenFallback can call this.
    /// @param from Token sender address.
    /// @param value Amount of tokens deposited to create the event.
    /// @param params The message data. First 4 bytes is function hash & rest is function params.
    /// @return New RewardEvent.
    function newRewardEvent(
        address from,
        uint value,
        bytes memory params)
        private
    {   
        // Validate escrow amount
        uint minEscrowAmount = IConfigManager(_configManager).eventEscrowAmount();
        require(value >= minEscrowAmount, "Escrow deposit is not enough");

        (string memory eventName, bytes32[] memory eventResults,
            uint betEndTime, uint resultSetStartTime, address centralizedOracle,
            uint8 arbitrationOptionIndex, uint arbitrationRewardPercentage) =
            abi.decode(params, (string, bytes32[], uint, uint, address, uint8,
            uint));

        // Create event
        RewardEvent newEvent = new RewardEvent(
            from, eventName, eventResults,
            value, betEndTime, resultSetStartTime, 
            centralizedOracle, arbitrationOptionIndex,
            arbitrationRewardPercentage, _configManager);

        // Transfer escrow to event
        INRC223(_bodhiTokenAddress).transfer(address(newEvent), value);

        emit RewardEventCreated(address(newEvent), from);
    }
}
