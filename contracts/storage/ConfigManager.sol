pragma solidity ^0.5.10;

import "./IConfigManager.sol";
import "../lib/Ownable.sol";

contract ConfigManager is IConfigManager, Ownable {
    uint16 private constant VERSION = 2;
    uint private constant TOKEN_DECIMALS = 8;

    uint private _eventEscrowAmount = 100 * (10 ** TOKEN_DECIMALS); // 100 NBOT
    uint[4] private _arbitrationLength = [
        172800, // 48 hours
        86400, // 24 hours
        43200, // 12 hours
        21600 // 6 hours
    ];
    uint[4] private _startingConsensusThreshold = [
        100 * (10 ** TOKEN_DECIMALS),
        1000 * (10 ** TOKEN_DECIMALS),
        5000 * (10 ** TOKEN_DECIMALS),
        10000 * (10 ** TOKEN_DECIMALS)
    ];
    uint private _thresholdPercentIncrease = 10;
    mapping(bytes32 => address) private _registeredAddresses;

    // Events
    event AddressRegistered(bytes32 indexed key, address indexed contractAddress);

    constructor() Ownable(msg.sender) public {
    }

    /// @dev Allows the owner to register an address.
    ///      Note: remember to keep the keys less than or equal to 32 chars.
    /// @param key Key to store the contract address at.
    /// @param contractAddress Contract address to register.
    function registerAddress(
        bytes32 key,
        address contractAddress)
        external
        onlyOwner
        validAddress(contractAddress)
    {
        _registeredAddresses[key] = contractAddress;
        emit AddressRegistered(key, contractAddress);
    }

    /// @dev Sets the escrow amount that is needed to create an Event.
    /// @param newAmount The new escrow amount needed to create an Event.
    function setEventEscrowAmount(
        uint newAmount)
        external
        onlyOwner
    {
        _eventEscrowAmount = newAmount;
    }

    /// @dev Sets the arbitration length.
    /// @param newLength New lengths of arbitration times (unix time seconds).
    function setArbitrationLength(
        uint[4] calldata newLength)
        external
        onlyOwner
    {   
        for (uint8 i = 0; i < 4; i++) {
            require(newLength[i] > 0, "Arbitration time should be > 0");
        }
        _arbitrationLength = newLength;
    }

    /// @dev Sets the starting betting threshold.
    /// @param newThreshold The new consensus threshold for the betting round.
    function setStartingConsensusThreshold(
        uint[4] calldata newThreshold)
        external
        onlyOwner
    {
        for (uint8 i = 0; i < 4; i++) {
            require(newThreshold[i] > 0, "Consensus threshold should be > 0");
        }
        _startingConsensusThreshold = newThreshold;
    }

    /// @dev Sets the threshold percentage increase.
    /// @param newPercentage The new percentage increase for each new round.
    function setConsensusThresholdPercentIncrease(
        uint newPercentage)
        external
        onlyOwner
    {
        _thresholdPercentIncrease = newPercentage;
    }

    function version() external pure returns (uint16) {
        return VERSION;
    }

    function getAddress(bytes32 key) external view returns (address) {
        return _registeredAddresses[key];
    }

    function eventEscrowAmount() external view returns (uint) {
        return _eventEscrowAmount;
    }

    function arbitrationLength() external view returns (uint[4] memory) {
        return _arbitrationLength;
    }

    function startingConsensusThreshold() external view returns (uint[4] memory) {
        return _startingConsensusThreshold;
    }

    function thresholdPercentIncrease() external view returns (uint) {
        return _thresholdPercentIncrease;
    }
}
