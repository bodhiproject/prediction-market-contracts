pragma solidity ^0.5.10;

contract IConfigManager {
    function getAddress(bytes32 key) external view returns (address);
    function eventEscrowAmount() external view returns (uint);
    function arbitrationLength() external view returns (uint[4] memory);
    function startingConsensusThreshold() external view returns (uint[4] memory);
    function thresholdPercentIncrease() external view returns (uint);
}
