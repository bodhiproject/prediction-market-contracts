# Changelog

## Current

## 8.0.2

- [#17] - Update reward and ab version

## 8.0.1

- [#16] - Fix winning calculation when no user invests on the final result

## 8.0.0

- [#9] - Add RewardEvent and RewardEventFactory
- [#12] - Add ConfigManager tests

## 7.0.0

- [#7] - Add MREvent and MREventFactory
