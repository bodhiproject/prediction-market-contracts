const { assert } = require('chai')
const TimeMachine = require('sol-time-machine')
const sassert = require('sol-assert')
const getConstants = require('../constants')
const ConfigManager = artifacts.require('ConfigManager')

const web3 = global.web3
const { toHex } = web3.utils;

contract('ConfigManager', (accounts) => {
  const {
    OWNER,
    ACCT1,
    INVALID_ADDR,
    MAX_GAS,
  } = getConstants(accounts)
  const timeMachine = new TimeMachine(web3)

  let contract
  let address
  let methods

  beforeEach(timeMachine.snapshot)
  afterEach(timeMachine.revert)

  beforeEach(async () => {
    contract = await ConfigManager.new({ from: OWNER, gas: MAX_GAS })
    address = contract.contract._address
    methods = contract.contract.methods
  })

  describe('constructor', () => {
    it('initializes all the values', async () => {
      assert.equal(await methods.owner().call(), OWNER)
    })
  })

  describe('registerAddress()', () => {
    it('allows owner to register an address', async () => {
      assert.equal(await methods.owner().call(), OWNER)
      
      const key = toHex('test')
      assert.equal(await methods.getAddress(key).call(), INVALID_ADDR)

      const registerAddr = '0x1234567890123456789012345678901234567890'
      const receipt = await methods.registerAddress(key, registerAddr)
        .send({ from: OWNER })
      assert.equal(await methods.getAddress(key).call(), registerAddr)

      sassert.event(receipt, 'AddressRegistered')
    })

    it('throws if a non-owner tries to register', async () => {
      try {
        await methods.registerAddress(
          toHex('test'),
          '0x1234567890123456789012345678901234567890',
        ).send({ from: ACCT1 })
      } catch (e) {
        sassert.revert(e)
      }
    })

    it('throws if trying to register an invalid address', async () => {
      try {
        await methods.registerAddress(
          toHex('test'),
          INVALID_ADDR,
        ).send({ from: OWNER })
      } catch (e) {
        sassert.revert(e)
      }
    })
  })

  describe('setEventEscrowAmount()', () => {
    it('allows owner to set the escrow amount', async () => {
      assert.equal(await methods.owner().call(), OWNER)
      sassert.bnEqual(await methods.eventEscrowAmount().call(), '10000000000')

      await methods.setEventEscrowAmount('1').send({ from: OWNER })
      sassert.bnEqual(await methods.eventEscrowAmount().call(), '1')
    })

    it('throws if a non-owner tries to set', async () => {
      try {
        await methods.setEventEscrowAmount('1').send({ from: ACCT1 })
      } catch (e) {
        sassert.revert(e)
      }
    })
  })

  describe('setArbitrationLength()', () => {
    it('allows owner to set the arbitration lengths', async () => {
      assert.equal(await methods.owner().call(), OWNER)
      assert.deepEqual(
        await methods.arbitrationLength().call(),
        ['172800', '86400', '43200', '21600'],
      )

      await methods.setArbitrationLength(['4', '3', '2', '1'])
        .send({ from: OWNER })
      assert.deepEqual(
        await methods.arbitrationLength().call(),
        ['4', '3', '2', '1'],
      )
    })

    it('throws if a non-owner tries to set', async () => {
      try {
        await methods.setArbitrationLength(['4', '3', '2', '1'])
          .send({ from: ACCT1 })
      } catch (e) {
        sassert.revert(e)
      }
    })

    it('throws if any of the amounts are 0', async () => {
      try {
        await methods.setArbitrationLength(['0', '3', '2', '1'])
          .send({ from: OWNER })
      } catch (e) {
        sassert.revert(e, 'Arbitration time should be > 0')
      }

      try {
        await methods.setArbitrationLength(['4', '0', '2', '1'])
          .send({ from: OWNER })
      } catch (e) {
        sassert.revert(e, 'Arbitration time should be > 0')
      }

      try {
        await methods.setArbitrationLength(['4', '3', '0', '1'])
          .send({ from: OWNER })
      } catch (e) {
        sassert.revert(e, 'Arbitration time should be > 0')
      }

      try {
        await methods.setArbitrationLength(['4', '3', '2', '0'])
          .send({ from: OWNER })
      } catch (e) {
        sassert.revert(e, 'Arbitration time should be > 0')
      }
    })
  })

  describe('setStartingConsensusThreshold()', () => {
    it('allows owner to set the consensus threshold', async () => {
      assert.equal(await methods.owner().call(), OWNER)
      assert.deepEqual(
        await methods.startingConsensusThreshold().call(),
        ['10000000000', '100000000000', '500000000000', '1000000000000'],
      )

      await methods.setStartingConsensusThreshold(['1', '2', '3', '4'])
        .send({ from: OWNER })
      assert.deepEqual(
        await methods.startingConsensusThreshold().call(),
        ['1', '2', '3', '4'],
      )
    })

    it('throws if a non-owner tries to set', async () => {
      try {
        await methods.setStartingConsensusThreshold(['1', '2', '3', '4'])
          .send({ from: ACCT1 })
      } catch (e) {
        sassert.revert(e)
      }
    })

    it('throws if any of the amount are 0', async () => {
      try {
        await methods.setStartingConsensusThreshold(['0', '3', '2', '1'])
          .send({ from: OWNER })
      } catch (e) {
        sassert.revert(e, 'Consensus threshold should be > 0')
      }

      try {
        await methods.setStartingConsensusThreshold(['4', '0', '2', '1'])
          .send({ from: OWNER })
      } catch (e) {
        sassert.revert(e, 'Consensus threshold should be > 0')
      }

      try {
        await methods.setStartingConsensusThreshold(['4', '3', '0', '1'])
          .send({ from: OWNER })
      } catch (e) {
        sassert.revert(e, 'Consensus threshold should be > 0')
      }

      try {
        await methods.setStartingConsensusThreshold(['4', '3', '2', '0'])
          .send({ from: OWNER })
      } catch (e) {
        sassert.revert(e, 'Consensus threshold should be > 0')
      }
    })
  })

  describe('setConsensusThresholdPercentIncrease()', () => {
    it('allows owner to set the threshold percent increase', async () => {
      assert.equal(await methods.owner().call(), OWNER)
      sassert.bnEqual(await methods.thresholdPercentIncrease().call(), '10')

      await methods.setConsensusThresholdPercentIncrease('1').send({ from: OWNER })
      sassert.bnEqual(await methods.thresholdPercentIncrease().call(), '1')
    })

    it('throws if a non-owner tries to set', async () => {
      try {
        await methods.setConsensusThresholdPercentIncrease('1')
          .send({ from: ACCT1 })
      } catch (e) {
        sassert.revert(e)
      }
    })
  })

  describe('version()', () => {
    it('returns the version', async () => {
      sassert.bnEqual(await methods.version().call(), 2)
    })
  })

  describe('getAddress()', () => {
    it('returns the address', async () => {
      assert.equal(await methods.getAddress(toHex('test')).call(), INVALID_ADDR)
    })
  })

  describe('eventEscrowAmount()', () => {
    it('returns the escrow amount', async () => {
      sassert.bnEqual(await methods.eventEscrowAmount().call(), '10000000000')
    })
  })

  describe('arbitrationLength()', () => {
    it('returns the arbitration lengths array', async () => {
      assert.deepEqual(
        await methods.arbitrationLength().call(),
        ['172800', '86400', '43200', '21600'],
      )
    })
  })

  describe('startingConsensusThreshold()', () => {
    it('returns the consensus threshold array', async () => {
      assert.deepEqual(
        await methods.startingConsensusThreshold().call(),
        ['10000000000', '100000000000', '500000000000', '1000000000000'],
      )
    })
  })

  describe('thresholdPercentIncrease()', () => {
    it('returns the threshold percent increase', async () => {
      sassert.bnEqual(await methods.thresholdPercentIncrease().call(), '10')
    })
  })
})
