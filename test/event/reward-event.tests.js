const { assert } = require('chai')
const TimeMachine = require('sol-time-machine')
const sassert = require('sol-assert')
const { isNumber, reduce } = require('lodash') 
const getConstants = require('../constants')
const {
  toSatoshi,
  currentBlockTime,
  constructTransfer223Data,
  decodeEvent,
} = require('../util')
const NRC223PreMinted = artifacts.require('NRC223PreMinted')
const ConfigManager = artifacts.require('ConfigManager')
const RewardEventFactory = artifacts.require('RewardEventFactory')
const RewardEvent = artifacts.require('RewardEvent')

const web3 = global.web3
const { toBN } = web3.utils;

const BODHI_TOKEN_KEY = web3.utils.toHex('bodhitoken')
const REWARD_FACTORY_KEY = web3.utils.toHex('rewardfactory')
const CREATE_EVENT_FUNC_SIG = '5280f033'
const BET_FUNC_SIG = '885ab66d'
const SET_RESULT_FUNC_SIG = 'a6b4218b'
const VOTE_FUNC_SIG = '1e00eb7f'
const DEPOSIT_FUNC_SIG = '47e7ef24'
const RESULT_INVALID = 'Invalid'
const DEFAULT_RESULT_INDEX = 255
const ORACLE_RESULT_SETTING_LENGTH = 172800

const fundUsers = async ({ nbotMethods, accounts }) => {
  await nbotMethods.transfer(accounts[1], toSatoshi(10000).toString())
    .send({ from: accounts[0] })
  await nbotMethods.transfer(accounts[2], toSatoshi(10000).toString())
    .send({ from: accounts[0] })
  await nbotMethods.transfer(accounts[3], toSatoshi(10000).toString())
    .send({ from: accounts[0] })
  await nbotMethods.transfer(accounts[4], toSatoshi(10000).toString())
    .send({ from: accounts[0] })
  await nbotMethods.transfer(accounts[5], toSatoshi(10000).toString())
    .send({ from: accounts[0] })
}

const getEventParams = async (cOracle, currTime) => {
  return [
    'Test Event 1',
    [
      web3.utils.toHex('A'),
      web3.utils.toHex('B'),
    ],
    currTime + 3000,
    currTime + 4000,
    cOracle,
    0,
    10,
  ]
}

const createEvent = async (
  { nbotMethods, eventParams, eventFactoryAddr, escrowAmt, from, gas }
) => {
  try {
    // Construct data
    const data = constructTransfer223Data(
      CREATE_EVENT_FUNC_SIG,
      ['string', 'bytes32[]', 'uint256', 'uint256', 'address', 'uint8', 'uint256'],
      eventParams,
    )

    // Send tx
    const receipt = await nbotMethods['transfer(address,uint256,bytes)'](
      eventFactoryAddr,
      escrowAmt,
      data,
    ).send({ from, gas })
  
    // Parse event log and instantiate event instance
    const decoded = decodeEvent(
      receipt.events,
      RewardEventFactory._json.abi,
      'RewardEventCreated'
    )
    // TODO: web3.eth.abi.decodeLog is parsing the logs backwards so it should
    // using eventAddress instead of ownerAddress
    return decoded.ownerAddress
  } catch (err) {
    throw err
  }
}

const placeBet = async (
  { nbotMethods, eventAddr, amtDecimals, amtSatoshi, resultIndex, from }
) => {
  const amt = isNumber(amtDecimals) ? toSatoshi(amtDecimals).toString() : amtSatoshi
  const data = constructTransfer223Data(BET_FUNC_SIG, ['uint8'], [resultIndex])
  await nbotMethods['transfer(address,uint256,bytes)'](
    eventAddr,
    amt,
    web3.utils.hexToBytes(data),
  ).send({ from, gas: 200000 })
}

const setResult = async (
  { nbotMethods, eventAddr, amt, resultIndex, from }
  ) => {
  const data = constructTransfer223Data(
    SET_RESULT_FUNC_SIG,
    ['uint8'],
    [resultIndex]
  )
  await nbotMethods['transfer(address,uint256,bytes)'](
    eventAddr,
    amt,
    web3.utils.hexToBytes(data),
  ).send({ from, gas: 4700000 })
}

const placeVote = async (
  { nbotMethods, eventAddr, amtDecimals, amtSatoshi, resultIndex, from }
) => {
  const amt = isNumber(amtDecimals) ? toSatoshi(amtDecimals).toString() : amtSatoshi
  const data = constructTransfer223Data(VOTE_FUNC_SIG, ['uint8'], [resultIndex])
  await nbotMethods['transfer(address,uint256,bytes)'](
    eventAddr,
    amt,
    web3.utils.hexToBytes(data),
  ).send({ from, gas: 400000 })
}

const placeDeposit = async (
  { nbotMethods, eventAddr, amtDecimals, amtSatoshi, from }
) => {
  const amt = isNumber(amtDecimals) ? toSatoshi(amtDecimals).toString() : amtSatoshi
  const data = constructTransfer223Data(DEPOSIT_FUNC_SIG, [], [])
  await nbotMethods['transfer(address,uint256,bytes)'](
    eventAddr,
    amt,
    web3.utils.hexToBytes(data),
  ).send({ from, gas: 200000 })
}

const calculateNormalWinnings = ({
  escrowAmt,
  myWinningBets,
  myLosingBets,
  betRoundWinnersTotal,
  voteRoundsWinnersTotal,
  voteRoundsLosersTotal,
  myWinningVotes
}) => {
  let betRoundWinningAmt = toBN(
    betRoundWinnersTotal
    .add(toBN(escrowAmt))
    .mul(myWinningBets)
    .div(betRoundWinnersTotal)
    .add(myLosingBets));

  // Calculate user's winning amount for vote rounds
  // use existing voteRoundLosersTotal calculation
  let voteRoundsWinningAmt = toBN(myWinningVotes
      .mul(voteRoundsLosersTotal)
      .div(voteRoundsWinnersTotal)
      .add(myWinningVotes));

  return {
    betRoundWinningAmt,
    voteRoundsWinningAmt
  }
}

calculateInvalidWinnings = ({
  playerWinningBets,
  playerLosingBets,
  playerWinningVotes,
  voteRoundsWinnersTotal,
  voteRoundsLosersTotal,
}) => {
  const betRoundWinningAmt = playerWinningBets.add(playerLosingBets)

  let voteRoundsWinningAmt = toBN(0)
  if (playerWinningVotes.gt(0) && voteRoundsWinnersTotal.gt(0)) {
    voteRoundsWinningAmt =
      voteRoundsLosersTotal
      .mul(playerWinningVotes)
      .div(voteRoundsWinnersTotal)
      .add(playerWinningVotes)
  }

  return {
    betRoundWinningAmt,
    voteRoundsWinningAmt,
  }
}

contract('RewardEvent', (accounts) => {
  const {
    OWNER,
    ACCT1,
    ACCT2,
    ACCT3,
    ACCT4,
    ACCT5,
    INVALID_ADDR,
    MAX_GAS,
  } = getConstants(accounts)
  const timeMachine = new TimeMachine(web3)

  let nbot
  let nbotAddr
  let nbotMethods
  let configManager
  let configManagerAddr
  let configManagerMethods
  let eventFactory
  let eventFactoryAddr
  let event
  let eventAddr
  let eventMethods
  let eventParams
  let escrowAmt
  let betStartTime
  let betEndTime
  let resultSetStartTime
  let resultSetEndTime

  beforeEach(timeMachine.snapshot)
  afterEach(timeMachine.revert)

  beforeEach(async () => {
    // Deploy token
    nbot = await NRC223PreMinted.new(
      'Naka Bodhi Token',
      'NBOT',
      8,
      '10000000000000000',
      OWNER,
      { from: OWNER, gas: MAX_GAS })
    nbotAddr = nbot.contract._address
    nbotMethods = nbot.contract.methods
    await fundUsers({ nbotMethods, accounts })

    // Deploy ConfigManager
    configManager = await ConfigManager.new({ from: OWNER, gas: MAX_GAS })
    configManagerAddr = configManager.contract._address
    configManagerMethods = configManager.contract.methods
    configManagerMethods.registerAddress(BODHI_TOKEN_KEY, nbotAddr)
      .send({ from: OWNER })
    escrowAmt = await configManagerMethods.eventEscrowAmount().call()

    // Deploy RewardEventFactory
    eventFactory = await RewardEventFactory.new(
      configManagerAddr,
      { from: OWNER, gas: MAX_GAS },
    )
    eventFactoryAddr = eventFactory.contract._address
    configManagerMethods.registerAddress(REWARD_FACTORY_KEY, eventFactoryAddr)
      .send({ from: OWNER })

    // Setup event params
    betStartTime = await currentBlockTime()
    eventParams = await getEventParams(OWNER, betStartTime)
    betEndTime = eventParams[2]
    resultSetStartTime = eventParams[3]
    resultSetEndTime = resultSetStartTime + ORACLE_RESULT_SETTING_LENGTH

    // NBOT.transfer() -> create event
    eventAddr = await createEvent({
      nbotMethods,
      eventParams,
      eventFactoryAddr,
      escrowAmt, 
      from: OWNER,
      gas: MAX_GAS,
    });
    
    event = await RewardEvent.at(eventAddr)
    eventMethods = event.contract.methods
  })

  describe('constructor', () => {
    it('initializes all the values', async () => {
      assert.equal(await eventMethods.owner().call(), OWNER)
      
      const eventMeta = await eventMethods.eventMetadata().call()
      assert.equal(eventMeta[0], 1)
      assert.equal(eventMeta[1], 'Test Event 1')
      assert.equal(web3.utils.toUtf8(eventMeta[2][0]), RESULT_INVALID)
      assert.equal(web3.utils.toUtf8(eventMeta[2][1]), 'A')
      assert.equal(web3.utils.toUtf8(eventMeta[2][2]), 'B')
      assert.equal(eventMeta[3], 3)

      const centralizedMeta = await eventMethods.centralizedMetadata().call()
      assert.equal(centralizedMeta[0], eventParams[4])
      // Block time during testing isn't completely accurate so test within a range
      assert.isTrue(centralizedMeta[1] >= betStartTime - 50
        && centralizedMeta[1] <= betStartTime + 50)
      assert.equal(centralizedMeta[2], betEndTime)
      assert.equal(centralizedMeta[3], resultSetStartTime)
      assert.equal(centralizedMeta[4], resultSetEndTime)

      const configMeta = await eventMethods.configMetadata().call()
      assert.equal(configMeta[0], escrowAmt)
      assert.equal(
        configMeta[1],
        (await configManagerMethods.arbitrationLength().call())[0],
      )
      assert.equal(
        configMeta[2],
        await configManagerMethods.thresholdPercentIncrease().call(),
      )
      assert.equal(configMeta[3], eventParams[6])
    })

    it('throws if centralizedOracle address is invalid', async () => {
      try {
        const params = await getEventParams(INVALID_ADDR, await currentBlockTime())
        params[0] = 'Test Event 2'
        await createEvent({
          nbotMethods,
          eventParams: params,
          eventFactoryAddr,
          escrowAmt,
          from: OWNER, 
          gas: MAX_GAS,
        })
      } catch (e) {
        sassert.revert(e)
      }
    })

    it('throws if eventName is empty', async () => {
      try {
        const params = await getEventParams(OWNER, await currentBlockTime())
        params[0] = ''
        await createEvent({
          nbotMethods,
          eventParams: params,
          eventFactoryAddr,
          escrowAmt,
          from: OWNER, 
          gas: MAX_GAS,
        })
      } catch (e) {
        sassert.revert(e, 'Event name cannot be empty')
      }
    })

    it('throws if eventResults 0 or 1 are empty', async () => {
      try {
        const params = await getEventParams(OWNER, await currentBlockTime())
        params[0] = 'Test Event 3'
        params[1] = [
          web3.utils.toHex(''),
          web3.utils.toHex('B'),
        ]
        await createEvent({
          nbotMethods,
          eventParams: params,
          eventFactoryAddr,
          escrowAmt,
          from: OWNER, 
          gas: MAX_GAS,
        })
      } catch (e) {
        sassert.revert(e, 'Event result cannot be empty')
      }

      try {
        const params = await getEventParams(OWNER, await currentBlockTime())
        params[0] = 'Test Event 4'
        params[1] = [
          web3.utils.toHex('A'),
          web3.utils.toHex(''),
        ]
        await createEvent({
          nbotMethods,
          eventParams: params,
          eventFactoryAddr,
          escrowAmt,
          from: OWNER, 
          gas: MAX_GAS,
        })
      } catch (e) {
        sassert.revert(e, 'Event result cannot be empty')
      }
    })

    it('throws if resultSetStartTime is < betEndTime', async () => {
      try {
        const params = await getEventParams(OWNER, await currentBlockTime())
        params[0] = 'Test Event 6'
        params[3] = params[2]
        await createEvent({
          nbotMethods,
          eventParams: params,
          eventFactoryAddr,
          escrowAmt,
          from: OWNER, 
          gas: MAX_GAS,
        })
      } catch (e) {
        sassert.revert(e, 'resultSetStartTime should be >= betEndTime')
      }
    })

    it('throws if arbitrationOptionIndex is invalid', async () => {
      try {
        const params = await getEventParams(OWNER, await currentBlockTime())
        params[0] = 'Test Event 7'
        params[5] = 4
        await createEvent({
          nbotMethods,
          eventParams: params,
          eventFactoryAddr,
          escrowAmt,
          from: OWNER, 
          gas: MAX_GAS,
        })
      } catch (e) {
        sassert.revert(e, 'arbitrationOptionIndex should be < 4')
      }
    })

    it('throws if arbitrationRewardPercentage is invalid', async () => {
      try {
        const params = await getEventParams(OWNER, await currentBlockTime())
        params[0] = 'Test Event 8'
        params[6] = 100
        await createEvent({
          nbotMethods,
          eventParams: params,
          eventFactoryAddr,
          escrowAmt,
          from: OWNER, 
          gas: MAX_GAS,
        })
      } catch (e) {
        sassert.revert(e, 'arbitrationRewardPercentage should be < 100')
      }
    })
  })

  describe('tokenFallback()', () => {
    it('throws if data is not long enough', async () => {
      try {
        await nbotMethods['transfer(address,uint256,bytes)'](
          eventAddr,
          1,
          web3.utils.hexToBytes('0xaabbcc'),
        ).send({ from: OWNER, gas: 200000 })
      } catch (e) {
        sassert.revert(e, 'Data is not long enough')
      }
    })

    it('throws if function sig is unhandled', async () => {
      try {
        await nbotMethods['transfer(address,uint256,bytes)'](
          eventAddr,
          1,
          web3.utils.hexToBytes('0xaabbccdd0000000000000000000000000000000000000000000000000000000000000001'),
        ).send({ from: OWNER, gas: 200000 })
      } catch (e) {
        sassert.revert(e, 'Unhandled function in tokenFallback')
      }
    })
  })

  describe('deposit()', () => {
    beforeEach(async () => {
      const currTime = await currentBlockTime()
      await timeMachine.increaseTime(betStartTime - currTime)
      assert.isAtLeast(await currentBlockTime(), betStartTime)
      assert.isBelow(await currentBlockTime(), betEndTime)
    })

    it('allows owner to deposit', async () => {
      const deposit1Amt = 1;
      await placeDeposit({
        nbotMethods,
        eventAddr,
        amtDecimals: deposit1Amt,
        from: OWNER,
      })
      escrowAmt = toBN(escrowAmt).add(toSatoshi(deposit1Amt));
      let data = await eventMethods.configMetadata().call();
      sassert.bnEqual(data[0], escrowAmt)
    })

    it('throws if the currentRound is not 0', async () => {
      const currTime = await currentBlockTime()
      await timeMachine.increaseTime(resultSetStartTime - currTime)
      assert.isAtLeast(await currentBlockTime(), resultSetStartTime)

      const amt = await eventMethods.currentConsensusThreshold().call()
      await setResult({
        nbotMethods,
        eventAddr,
        amt,
        resultIndex: 1,
        from: OWNER,
      })
      assert.equal(await eventMethods.currentRound().call(), 1)

      try {
        await placeDeposit({
          nbotMethods,
          eventAddr,
          amtDecimals: 1,
          from: OWNER,
        })
      } catch (e) {
        sassert.revert(e, 'Cannot deposit after betting round')
      }
    })

    it('throws if depositor is not the owner', async () => {
      try {
        await placeDeposit({
          nbotMethods,
          eventAddr,
          amtDecimals: 1,
          from: ACCT1,
        })
      } catch (e) {
        sassert.revert(e, 'Only owner can deposit')
      }
    })

    it('throws if the deposit amount is 0', async () => {
      try {
        await placeDeposit({
          nbotMethods,
          eventAddr,
          amtDecimals: 0,
          from: OWNER,
        })
      } catch (e) {
        sassert.revert(e, 'Escrow deposit is not larger than 0')
      }
    })

    it('throws if deposit after betEndTime', async () => {
      const currTime = await currentBlockTime()
      await timeMachine.increaseTime(betEndTime - currTime)
      assert.isAtLeast(await currentBlockTime(), betEndTime)
      try {
        await placeDeposit({
          nbotMethods,
          eventAddr,
          amtDecimals: 2,
          from: OWNER,
        })
      } catch (e) {
        sassert.revert(e, 'Current time should be < betEndTime')
      }
    })
  })

  describe('bet()', () => {
    describe('valid time', () => {
      beforeEach(async () => {
        const currTime = await currentBlockTime()
        await timeMachine.increaseTime(betStartTime - currTime)
        assert.isAtLeast(await currentBlockTime(), betStartTime)
        assert.isBelow(await currentBlockTime(), betEndTime)
      })

      it('allows users to bet', async () => {
        const bet1Amt = 1;
        await placeBet({
          nbotMethods,
          eventAddr,
          amtDecimals: bet1Amt,
          resultIndex: 1,
          from: OWNER,
        })
        sassert.bnEqual(await eventMethods.totalBets().call(), toSatoshi(bet1Amt))
  
        const bet2Amt = 1;
        await placeBet({
          nbotMethods,
          eventAddr,
          amtDecimals: bet2Amt,
          resultIndex: 1,
          from: ACCT1,
        })
        sassert.bnEqual(
          await eventMethods.totalBets().call(),
          toSatoshi(bet1Amt + bet2Amt))
      })
  
      it('throws if the currentRound is not 0', async () => {
        const currTime = await currentBlockTime()
        await timeMachine.increaseTime(resultSetStartTime - currTime)
        assert.isAtLeast(await currentBlockTime(), resultSetStartTime)

        const amt = await eventMethods.currentConsensusThreshold().call()
        await setResult({
          nbotMethods,
          eventAddr,
          amt,
          resultIndex: 1,
          from: OWNER,
        })
        assert.equal(await eventMethods.currentRound().call(), 1)

        try {
          await placeBet({
            nbotMethods,
            eventAddr,
            amtDecimals: 1,
            resultIndex: 2,
            from: OWNER,
          })
        } catch (e) {
          sassert.revert(e, 'Can only bet during the betting round')
        }
      })

      it('throws if the resultIndex is invalid', async () => {
        try {
          await placeBet({
            nbotMethods,
            eventAddr,
            amtDecimals: 1,
            resultIndex: 4,
            from: OWNER,
          })
        } catch (e) {
          sassert.revert(e, 'resultIndex is not valid')
        }
      })

      it('throws if the bet amount is 0', async () => {
        try {
          await placeBet({
            nbotMethods,
            eventAddr,
            amtDecimals: 0,
            resultIndex: 1,
            from: OWNER,
          })
        } catch (e) {
          sassert.revert(e, 'Bet amount should be > 0')
        }
      })
    })

    describe('invalid time', () => {
      it('throws if the current time is > betEndTime', async () => {
        const currTime = await currentBlockTime()
        await timeMachine.increaseTime(betEndTime - currTime)
        assert.isAtLeast(await currentBlockTime(), betEndTime)

        try {
          await placeBet({
            nbotMethods,
            eventAddr,
            amtDecimals: 1,
            resultIndex: 1,
            from: OWNER,
          })
        } catch (e) {
          sassert.revert(e, 'Current time should be < betEndTime.')
        }
      })
    })
  })

  describe('setResult()', () => {
    let threshold

    beforeEach(async () => {
      threshold = await eventMethods.currentConsensusThreshold().call()
    })

    describe('valid time', () => {
      beforeEach(async () => {
        const currTime = await currentBlockTime()
        await timeMachine.increaseTime(resultSetStartTime - currTime)
        assert.isAtLeast(await currentBlockTime(), resultSetStartTime)
      })

      it('sets the result', async () => {
        assert.equal(
          await eventMethods.currentResultIndex().call(),
          DEFAULT_RESULT_INDEX)

        await setResult({
          nbotMethods,
          eventAddr,
          amt: threshold,
          resultIndex: 1,
          from: OWNER,
        })
        assert.equal(await eventMethods.currentResultIndex().call(), 1)
        assert.equal(await eventMethods.currentRound().call(), 1)
        assert.equal(await eventMethods.totalBets().call(), threshold)
        sassert.bnGTE(
          await eventMethods.currentConsensusThreshold().call(),
          threshold)
        sassert.bnGTE(
          await eventMethods.currentArbitrationEndTime().call(),
          resultSetEndTime)
      })

      it('allows anyone to set the result after the resultSetEndTime', async () => {
        const currTime = await currentBlockTime()
        await timeMachine.increaseTime(resultSetEndTime - currTime)
        assert.isAtLeast(await currentBlockTime(), resultSetEndTime)

        assert.equal(
          await eventMethods.currentResultIndex().call(),
          DEFAULT_RESULT_INDEX)

        await setResult({
          nbotMethods,
          eventAddr,
          amt: threshold,
          resultIndex: 1,
          from: ACCT1,
        })
        assert.equal(await eventMethods.currentResultIndex().call(), 1)
        assert.equal(await eventMethods.currentRound().call(), 1)
        assert.equal(await eventMethods.totalBets().call(), threshold)
        sassert.bnGTE(
          await eventMethods.currentConsensusThreshold().call(),
          threshold)
        sassert.bnGTE(
          await eventMethods.currentArbitrationEndTime().call(),
          resultSetEndTime)
      })

      it('throws if the resultIndex is invalid', async () => {
        try {
          await setResult({
            nbotMethods,
            eventAddr,
            amt: threshold,
            resultIndex: 4,
            from: OWNER,
          })
        } catch (e) {
          sassert.revert(e, 'resultIndex is not valid')
        }
      })
  
      it('throws if the currentRound is not 0', async () => {
        await setResult({
          nbotMethods,
          eventAddr,
          amt: threshold,
          resultIndex: 1,
          from: OWNER,
        })
        assert.equal(await eventMethods.currentRound().call(), 1)

        try {
          await setResult({
            nbotMethods,
            eventAddr,
            amt: threshold,
            resultIndex: 2,
            from: OWNER,
          })
        } catch (e) {
          sassert.revert(e, 'Can only set result during the betting round')
        }
      })

      it('throws if a non-centralized oracle sets the result during oracle result setting', async () => {
        assert.isBelow(await currentBlockTime(), resultSetEndTime)

        try {
          await setResult({
            nbotMethods,
            eventAddr,
            amt: threshold,
            resultIndex: 1,
            from: ACCT1,
          })
        } catch (e) {
          sassert.revert(e, 'Only the Centralized Oracle can set the result')
        }
      })

      it('throws if the value is not the consensus threshold', async () => {
        try {
          await setResult({
            nbotMethods,
            eventAddr,
            amt: toBN(threshold).sub(toBN(1)).toString(),
            resultIndex: 1,
            from: OWNER,
          })
        } catch (e) {
          sassert.revert(e, 'Set result amount should = consensusThreshold')
        }

        try {
          await setResult({
            nbotMethods,
            eventAddr,
            amt: toBN(threshold).add(toBN(1)).toString(),
            resultIndex: 1,
            from: OWNER,
          })
        } catch (e) {
          sassert.revert(e, 'Set result amount should = consensusThreshold')
        }
      })
    })

    describe('invalid time', () => {
      it('throws if the current time is < resultSetStartTime', async () => {
        assert.isBelow(await currentBlockTime(), resultSetStartTime)

        try {
          await setResult({
            nbotMethods,
            eventAddr,
            amt: threshold,
            resultIndex: 1,
            from: OWNER,
          })
        } catch (e) {
          sassert.revert(e, 'Current time should be >= resultSetStartTime')
        }
      })
    })
  })

  describe('vote()', () => {
    let threshold

    describe('valid time', () => {
      beforeEach(async () => {
        const currTime = await currentBlockTime()
        await timeMachine.increaseTime(resultSetStartTime - currTime)
        assert.isAtLeast(await currentBlockTime(), resultSetStartTime)

        threshold = await eventMethods.currentConsensusThreshold().call()
        await setResult({
          nbotMethods,
          eventAddr,
          amt: threshold,
          resultIndex: 1,
          from: OWNER,
        })
        assert.equal(await eventMethods.currentResultIndex().call(), 1)
        assert.equal(await eventMethods.currentRound().call(), 1)
        assert.isBelow(
          await currentBlockTime(),
          Number(await eventMethods.currentArbitrationEndTime().call()))
      })

      it('allows voting', async () => {
        let amt = 1
        await placeVote({
          nbotMethods,
          eventAddr,
          amtDecimals: amt,
          resultIndex: 2,
          from: ACCT1,
        })
        let totalBets = toBN(threshold).add(toSatoshi(amt))
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        amt = 2
        await placeVote({
          nbotMethods,
          eventAddr,
          amtDecimals: amt,
          resultIndex: 2,
          from: ACCT2,
        })
        totalBets = totalBets.add(toSatoshi(amt))
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)
      })

      it('sets the result if voting to the threshold', async () => {
        const amt = await eventMethods.currentConsensusThreshold().call()
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: amt,
          resultIndex: 2,
          from: ACCT1,
        })
        let totalBets = toBN(threshold).add(toBN(amt))
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)
        assert.equal(await eventMethods.currentResultIndex().call(), 2)
        assert.equal(await eventMethods.currentRound().call(), 2)
      })

      it('refunds the diff over the threshold', async () => {
        const balance = toBN(await nbotMethods.balanceOf(ACCT1).call())
        const amt = toBN(await eventMethods.currentConsensusThreshold().call())
        const diff = toSatoshi(25)
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: amt.add(diff).toString(),
          resultIndex: 2,
          from: ACCT1,
        })
        sassert.bnEqual(
          await eventMethods.totalBets().call(),
          toBN(threshold).add(amt))
        assert.equal(await eventMethods.currentResultIndex().call(), 2)
        assert.equal(await eventMethods.currentRound().call(), 2)
        sassert.bnEqual(await nbotMethods.balanceOf(ACCT1).call(), balance.sub(amt))
      })

      it('throws if the resultIndex is invalid', async () => {
        try {
          await placeVote({
            nbotMethods,
            eventAddr,
            amtDecimals: 1,
            resultIndex: 4,
            from: ACCT1,
          })
        } catch (e) {
          sassert.revert(e, 'resultIndex is not valid')
        }
      })

      it('throws if the current time is past the arbitrationEndTime', async () => {
        const currTime = await currentBlockTime()
        const arbEndTime = Number(await eventMethods.currentArbitrationEndTime().call())
        await timeMachine.increaseTime(arbEndTime - currTime)
        assert.isAtLeast(await currentBlockTime(), arbEndTime)

        try {
          await placeVote({
            nbotMethods,
            eventAddr,
            amtDecimals: 1,
            resultIndex: 2,
            from: ACCT1,
          })
        } catch (e) {
          sassert.revert(e, 'Current time should be < arbitrationEndTime')
        }
      })

      it('throws if voting on the last result index', async () => {
        assert.equal(await eventMethods.currentResultIndex().call(), 1)

        try {
          await placeVote({
            nbotMethods,
            eventAddr,
            amtDecimals: 1,
            resultIndex: 1,
            from: ACCT1,
          })
        } catch (e) {
          sassert.revert(e, 'Cannot vote on the last result index')
        }
      })

      it('throws if the vote amount is 0', async () => {
        try {
          await placeVote({
            nbotMethods,
            eventAddr,
            amtDecimals: 0,
            resultIndex: 2,
            from: ACCT1,
          })
        } catch (e) {
          sassert.revert(e, 'Vote amount should be > 0')
        }
      })
    })

    describe('invalid time', () => {
      it('throws if trying to vote in round 0', async () => {
        assert.equal(await eventMethods.currentRound().call(), 0)

        try {
          await placeVote({
            nbotMethods,
            eventAddr,
            amtDecimals: 1,
            resultIndex: 1,
            from: ACCT1,
          })
        } catch (e) {
          sassert.revert(e, 'Can only vote after the betting round')
        }
      })
    })
  })

  describe('withdraw()', () => {
    it('withdraws the winning amount', async () => {
      const cOracleResult = 1
      let totalBets

      // Advance to betting time
      let currTime = await currentBlockTime()
      await timeMachine.increaseTime(betStartTime - currTime)
      assert.isAtLeast(await currentBlockTime(), betStartTime)
      assert.isBelow(await currentBlockTime(), betEndTime)

      // First round of betting
      const bet1 = toSatoshi(50)
      await placeBet({
        nbotMethods,
        eventAddr,
        amtSatoshi: bet1.toString(),
        resultIndex: 1,
        from: ACCT1,
      })
      totalBets = bet1
      sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

      const bet2 = toSatoshi(10)
      await placeBet({
        nbotMethods,
        eventAddr,
        amtSatoshi: bet2.toString(),
        resultIndex: 2,
        from: ACCT2,
      })
      totalBets = totalBets.add(bet2)
      sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

      // Advance to result setting time
      currTime = await currentBlockTime()
      await timeMachine.increaseTime(resultSetStartTime - currTime)
      assert.isAtLeast(await currentBlockTime(), resultSetStartTime)

      // Set result 2
      const cOracleThreshold =
        toBN(await eventMethods.currentConsensusThreshold().call())
      await setResult({
        nbotMethods,
        eventAddr,
        amt: cOracleThreshold.toString(),
        resultIndex: cOracleResult,
        from: OWNER,
      })
      totalBets = totalBets.add(cOracleThreshold)
      sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)
      assert.equal(await eventMethods.currentResultIndex().call(), cOracleResult)
      assert.equal(await eventMethods.currentRound().call(), 1)

      // Advance to arbitration end time
      currTime = await currentBlockTime()
      const arbEndTime = Number(await eventMethods.currentArbitrationEndTime().call())
      await timeMachine.increaseTime(arbEndTime - currTime)
      assert.isAtLeast(await currentBlockTime(), arbEndTime)

      let balance = toBN(await nbotMethods.balanceOf(eventAddr).call())
      
      // ACCT1 winner withdraws
      let winningAmt = toBN(await eventMethods.calculateWinnings(ACCT1).call())
      assert.isTrue(winningAmt.toNumber() > 0)
      let receipt = await eventMethods.withdraw().send({ from: ACCT1, gas: 200000 })
      sassert.event(receipt, 'WinningsWithdrawn')
      assert.isTrue(await eventMethods.didWithdraw(ACCT1).call())
      sassert.bnEqual(
        await nbotMethods.balanceOf(eventAddr).call(),
        balance.sub(winningAmt))
      balance = balance.sub(winningAmt)

      // OWNER winner withdraws winning amount and escrow
      const ownerBal = toBN(await nbotMethods.balanceOf(OWNER).call())
      winningAmt = toBN(await eventMethods.calculateWinnings(OWNER).call())
      assert.isTrue(winningAmt.toNumber() > 0)
      receipt = await eventMethods.withdraw().send({ from: OWNER, gas: 200000 })
      sassert.event(receipt, 'WinningsWithdrawn')
      assert.isTrue(await eventMethods.didWithdraw(OWNER).call())
      sassert.bnEqual(
        await nbotMethods.balanceOf(eventAddr).call(),
        balance.sub(winningAmt))
      sassert.bnEqual(
        await nbotMethods.balanceOf(OWNER).call(),
        ownerBal.add(winningAmt))

      // ACCT2 takes losing bets back
      balance = balance.sub(winningAmt)
      winningAmt = toBN(await eventMethods.calculateWinnings(ACCT2).call())
      assert.isTrue(winningAmt.toNumber() > 0)
      receipt = await eventMethods.withdraw().send({ from: ACCT2, gas: 200000 })
      sassert.event(receipt, 'WinningsWithdrawn')
      assert.isTrue(await eventMethods.didWithdraw(ACCT2).call())
      sassert.bnEqual(
        await nbotMethods.balanceOf(eventAddr).call(),
        balance.sub(winningAmt))

      // Contract should be empty
      assert.equal(await nbotMethods.balanceOf(eventAddr).call(), 0)
    })

    it('withdraws the winning amount when result is invalid', async () => {
      const cOracleResult = 0
      let totalBets

      // Advance to betting time
      let currTime = await currentBlockTime()
      await timeMachine.increaseTime(betStartTime - currTime)
      assert.isAtLeast(await currentBlockTime(), betStartTime)
      assert.isBelow(await currentBlockTime(), betEndTime)

      // First round of betting
      const bet1 = toSatoshi(50)
      await placeBet({
        nbotMethods,
        eventAddr,
        amtSatoshi: bet1.toString(),
        resultIndex: 1,
        from: ACCT1,
      })
      totalBets = bet1
      sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

      const bet2 = toSatoshi(10)
      await placeBet({
        nbotMethods,
        eventAddr,
        amtSatoshi: bet2.toString(),
        resultIndex: 2,
        from: ACCT2,
      })
      totalBets = totalBets.add(bet2)
      sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

      // Advance to result setting time
      currTime = await currentBlockTime()
      await timeMachine.increaseTime(resultSetStartTime - currTime)
      assert.isAtLeast(await currentBlockTime(), resultSetStartTime)

      // Set result 2
      const cOracleThreshold =
        toBN(await eventMethods.currentConsensusThreshold().call())
      await setResult({
        nbotMethods,
        eventAddr,
        amt: cOracleThreshold.toString(),
        resultIndex: cOracleResult,
        from: OWNER,
      })
      totalBets = totalBets.add(cOracleThreshold)
      sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)
      assert.equal(await eventMethods.currentResultIndex().call(), cOracleResult)
      assert.equal(await eventMethods.currentRound().call(), 1)

      // Advance to arbitration end time
      currTime = await currentBlockTime()
      const arbEndTime = Number(await eventMethods.currentArbitrationEndTime().call())
      await timeMachine.increaseTime(arbEndTime - currTime)
      assert.isAtLeast(await currentBlockTime(), arbEndTime)

      let balance = toBN(await nbotMethods.balanceOf(eventAddr).call())

      // ACCT1 winner withdraws
      let winningAmt = toBN(await eventMethods.calculateWinnings(ACCT1).call())
      assert.isTrue(winningAmt.toNumber() > 0)
      let receipt = await eventMethods.withdraw().send({ from: ACCT1, gas: 200000 })
      sassert.event(receipt, 'WinningsWithdrawn')
      assert.isTrue(await eventMethods.didWithdraw(ACCT1).call())
      
      sassert.bnEqual(
        await nbotMethods.balanceOf(eventAddr).call(),
        balance.sub(winningAmt))
      balance = balance.sub(winningAmt)

      // OWNER winner withdraws winning amount and escrow
      const ownerBal = toBN(await nbotMethods.balanceOf(OWNER).call())
      winningAmt = toBN(await eventMethods.calculateWinnings(OWNER).call())
      assert.isTrue(winningAmt.toNumber() > 0)
      receipt = await eventMethods.withdraw().send({ from: OWNER, gas: 200000 })
      sassert.event(receipt, 'WinningsWithdrawn')
      assert.isTrue(await eventMethods.didWithdraw(OWNER).call())
      sassert.bnEqual(
        await nbotMethods.balanceOf(eventAddr).call(),
        balance.sub(winningAmt).sub(toBN(escrowAmt)))
      sassert.bnEqual(
        await nbotMethods.balanceOf(OWNER).call(),
        ownerBal.add(winningAmt).add(toBN(escrowAmt)))

      // Contract should be left with bet 2
      assert.equal(await nbotMethods.balanceOf(eventAddr).call(), bet2)
    })

    it('withdraws the winning amount when no one invests into the result', async () => {
      const cOracleResult = 1
      let totalBets

      // Advance to betting time
      let currTime = await currentBlockTime()
      await timeMachine.increaseTime(betStartTime - currTime)
      assert.isAtLeast(await currentBlockTime(), betStartTime)
      assert.isBelow(await currentBlockTime(), betEndTime)

      // First round of betting
      const bet1 = toSatoshi(50)
      await placeBet({
        nbotMethods,
        eventAddr,
        amtSatoshi: bet1.toString(),
        resultIndex: 2,
        from: ACCT1,
      })
      totalBets = bet1
      sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

      const bet2 = toSatoshi(10)
      await placeBet({
        nbotMethods,
        eventAddr,
        amtSatoshi: bet2.toString(),
        resultIndex: 2,
        from: ACCT2,
      })
      totalBets = totalBets.add(bet2)
      sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

      // Advance to result setting time
      currTime = await currentBlockTime()
      await timeMachine.increaseTime(resultSetStartTime - currTime)
      assert.isAtLeast(await currentBlockTime(), resultSetStartTime)

      // Set result 1
      const cOracleThreshold =
        toBN(await eventMethods.currentConsensusThreshold().call())
      await setResult({
        nbotMethods,
        eventAddr,
        amt: cOracleThreshold.toString(),
        resultIndex: cOracleResult,
        from: OWNER,
      })
      totalBets = totalBets.add(cOracleThreshold)
      sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)
      assert.equal(await eventMethods.currentResultIndex().call(), cOracleResult)
      assert.equal(await eventMethods.currentRound().call(), 1)

      // Advance to arbitration end time
      currTime = await currentBlockTime()
      const arbEndTime = Number(await eventMethods.currentArbitrationEndTime().call())
      await timeMachine.increaseTime(arbEndTime - currTime)
      assert.isAtLeast(await currentBlockTime(), arbEndTime)

      let balance = toBN(await nbotMethods.balanceOf(eventAddr).call())

      // ACCT1 withdraws losing bet
      let winningAmt = toBN(await eventMethods.calculateWinnings(ACCT1).call())
      assert.isTrue(winningAmt.toNumber() > 0)
      let receipt = await eventMethods.withdraw().send({ from: ACCT1, gas: 200000 })
      sassert.event(receipt, 'WinningsWithdrawn')
      assert.isTrue(await eventMethods.didWithdraw(ACCT1).call())
      
      sassert.bnEqual(
        await nbotMethods.balanceOf(eventAddr).call(),
        balance.sub(winningAmt))
      sassert.bnEqual(
        winningAmt,
        bet1)
      balance = balance.sub(winningAmt)

      // OWNER winner withdraws winning amount and escrow
      const ownerBal = toBN(await nbotMethods.balanceOf(OWNER).call())
      winningAmt = toBN(await eventMethods.calculateWinnings(OWNER).call())
      assert.isTrue(winningAmt.toNumber() > 0)
      receipt = await eventMethods.withdraw().send({ from: OWNER, gas: 200000 })
      sassert.event(receipt, 'WinningsWithdrawn')
      assert.isTrue(await eventMethods.didWithdraw(OWNER).call())
      sassert.bnEqual(
        await nbotMethods.balanceOf(eventAddr).call(),
        balance.sub(winningAmt).sub(toBN(escrowAmt)))
      sassert.bnEqual(
        await nbotMethods.balanceOf(OWNER).call(),
        ownerBal.add(winningAmt).add(toBN(escrowAmt)))
      balance = balance.sub(winningAmt).sub(toBN(escrowAmt))

      // ACCT2 withdraws losing bet
      winningAmt = toBN(await eventMethods.calculateWinnings(ACCT2).call())
      assert.isTrue(winningAmt.toNumber() > 0)
      receipt = await eventMethods.withdraw().send({ from: ACCT2, gas: 200000 })
      sassert.event(receipt, 'WinningsWithdrawn')
      assert.isTrue(await eventMethods.didWithdraw(ACCT2).call())
      
      sassert.bnEqual(
        await nbotMethods.balanceOf(eventAddr).call(),
        balance.sub(winningAmt))
      sassert.bnEqual(
        winningAmt,
        bet2)
      balance = balance.sub(winningAmt)

      // Contract should be left with 0
      assert.equal(await nbotMethods.balanceOf(eventAddr).call(), 0)
    })

    it('throws if trying to withdraw during round 0', async () => {
      assert.equal(await eventMethods.currentRound().call(), 0)

      try {
        await eventMethods.withdraw().send({ from: OWNER, gas: 200000 })
      } catch (e) {
        sassert.revert(e, 'Cannot withdraw during betting round.')
      }
    })

    it('throws if trying to withdraw before the arbitrationEndTime', async () => {
      // Advance to result setting time
      let currTime = await currentBlockTime()
      await timeMachine.increaseTime(resultSetStartTime - currTime)
      assert.isAtLeast(await currentBlockTime(), resultSetStartTime)

      // Set result
      const cOracleThreshold =
        toBN(await eventMethods.currentConsensusThreshold().call())
      await setResult({
        nbotMethods,
        eventAddr,
        amt: cOracleThreshold.toString(),
        resultIndex: 1,
        from: OWNER,
      })
      assert.equal(await eventMethods.currentResultIndex().call(), 1)
      assert.equal(await eventMethods.currentRound().call(), 1)

      // Check if under arb end time
      const arbEndTime = Number(await eventMethods.currentArbitrationEndTime().call())
      assert.isBelow(await currentBlockTime(), arbEndTime)

      try {
        await eventMethods.withdraw().send({ from: OWNER, gas: 200000 })
      } catch (e) {
        sassert.revert(e, 'Current time should be >= arbitrationEndTime')
      }
    })

    it('throws if trying to withdraw more than once', async () => {
      // Advance to result setting time
      let currTime = await currentBlockTime()
      await timeMachine.increaseTime(resultSetStartTime - currTime)
      assert.isAtLeast(await currentBlockTime(), resultSetStartTime)

      // Set result
      const cOracleThreshold =
        toBN(await eventMethods.currentConsensusThreshold().call())
      await setResult({
        nbotMethods,
        eventAddr,
        amt: cOracleThreshold.toString(),
        resultIndex: 1,
        from: OWNER,
      })
      assert.equal(await eventMethods.currentResultIndex().call(), 1)
      assert.equal(await eventMethods.currentRound().call(), 1)

      // Advance to arbitration end time
      currTime = await currentBlockTime()
      const arbEndTime = Number(await eventMethods.currentArbitrationEndTime().call())
      await timeMachine.increaseTime(arbEndTime - currTime)
      assert.isAtLeast(await currentBlockTime(), arbEndTime)
      // Withdraw once
      await eventMethods.withdraw().send({ from: OWNER, gas: 200000 })
      assert.isTrue(await eventMethods.didWithdraw(OWNER).call())
      
      try {
        await eventMethods.withdraw().send({ from: OWNER, gas: 200000 })
      } catch (e) {
        sassert.revert(e, 'Already withdrawn')
      }
    })
  })

  describe('winnings calculations', () => {
    describe('non-Invalid resultIndex scenario', () => {
      const cOracleResult = 2
      const dOracle1Result = 1
      const dOracle2Result = 2
      let bet1
      let bet2
      let bet3
      let bet4
      let cOracleThreshold
      let vote1a
      let vote2a
      let vote3a
      let vote4a
      let vote5a
      let vote1b
      let vote2b
      let calcParams

      beforeEach(async () => {
        let totalBets

        // Advance to betting time
        let currTime = await currentBlockTime()
        await timeMachine.increaseTime(betStartTime - currTime)
        assert.isAtLeast(await currentBlockTime(), betStartTime)
        assert.isBelow(await currentBlockTime(), betEndTime)

        // First round of betting
        bet1 = toSatoshi(12)
        await placeBet({
          nbotMethods,
          eventAddr,
          amtSatoshi: bet1.toString(),
          resultIndex: 1,
          from: ACCT1,
        })
        totalBets = bet1
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        bet2 = toSatoshi(23)
        await placeBet({
          nbotMethods,
          eventAddr,
          amtSatoshi: bet2.toString(),
          resultIndex: 1,
          from: ACCT2,
        })
        totalBets = totalBets.add(bet2)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        bet3 = toSatoshi(30)
        await placeBet({
          nbotMethods,
          eventAddr,
          amtSatoshi: bet3.toString(),
          resultIndex: cOracleResult,
          from: ACCT3,
        })
        totalBets = totalBets.add(bet3)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        bet4 = toSatoshi(5)
        await placeBet({
          nbotMethods,
          eventAddr,
          amtSatoshi: bet4.toString(),
          resultIndex: cOracleResult,
          from: ACCT4,
        })
        totalBets = totalBets.add(bet4)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        // Advance to result setting time
        currTime = await currentBlockTime()
        await timeMachine.increaseTime(resultSetStartTime - currTime)
        assert.isAtLeast(await currentBlockTime(), resultSetStartTime)

        // Set result 2
        cOracleThreshold =
          toBN(await eventMethods.currentConsensusThreshold().call())
        await setResult({
          nbotMethods,
          eventAddr,
          amt: cOracleThreshold.toString(),
          resultIndex: cOracleResult,
          from: OWNER,
        })
        
        totalBets = totalBets.add(cOracleThreshold)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)
        assert.equal(await eventMethods.currentResultIndex().call(), cOracleResult)
        assert.equal(await eventMethods.currentRound().call(), 1)

        // dOracle1 voting. Threshold hits and result becomes 1.
        vote1a = toSatoshi(60)
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: vote1a.toString(),
          resultIndex: dOracle1Result,
          from: ACCT1,
        })
        totalBets = totalBets.add(vote1a)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        vote2a = toSatoshi(50)
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: vote2a.toString(),
          resultIndex: dOracle1Result,
          from: ACCT2,
        })
        totalBets = totalBets.add(vote2a)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)
        assert.equal(await eventMethods.currentResultIndex().call(), dOracle1Result)
        assert.equal(await eventMethods.currentRound().call(), 2)
        
        // dOracle2 voting. Threshold hits and result becomes 2.
        vote3a = toSatoshi(41)
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: vote3a.toString(),
          resultIndex: dOracle2Result,
          from: ACCT3,
        })
        totalBets = totalBets.add(vote3a)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)
        vote4a = toSatoshi(43)
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: vote4a.toString(),
          resultIndex: dOracle2Result,
          from: ACCT4,
        })
        totalBets = totalBets.add(vote4a)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        vote5a = toSatoshi(37)
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: vote5a.toString(),
          resultIndex: dOracle2Result,
          from: ACCT5,
        })
        totalBets = totalBets.add(vote5a)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)
        assert.equal(await eventMethods.currentResultIndex().call(), dOracle2Result)
        assert.equal(await eventMethods.currentRound().call(), 3)

        // dOracle3 voting. Does not hit threshold and result gets finalized to 2.
        vote1b = toSatoshi(53)
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: vote1b.toString(),
          resultIndex: dOracle1Result,
          from: ACCT1,
        })
        totalBets = totalBets.add(vote1b)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        vote2b = toSatoshi(49)
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: vote2b.toString(),
          resultIndex: dOracle1Result,
          from: ACCT2,
        })
        
        totalBets = totalBets.add(vote2b)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        // Advance to arbitration end time
        currTime = await currentBlockTime()
        const arbEndTime = Number(await eventMethods.currentArbitrationEndTime().call())
        await timeMachine.increaseTime(arbEndTime - currTime)
        assert.isAtLeast(await currentBlockTime(), arbEndTime)

        // Calculate totals
        const maxPercent = toBN(100)
        const arbRewardPercent = toBN((await eventMethods.configMetadata().call())[3])
        const arbRewardPercentComp = maxPercent.sub(arbRewardPercent)
        const betRoundWinnersTotal = bet3.add(bet4)
        const betRoundLosersTotal = bet1.add(bet2)
        const voteRoundsWinnersTotal = cOracleThreshold.add(vote3a).add(vote4a).add(vote5a)
        const voteRoundsLosersTotal = vote1a.add(vote2a).add(vote1b).add(vote2b)
        calcParams = {
          maxPercent,
          arbRewardPercent,
          arbRewardPercentComp,
          betRoundWinnersTotal,
          betRoundLosersTotal,
          voteRoundsWinnersTotal,
          voteRoundsLosersTotal,
        }
      })

      describe('calculateWinnings', () => {
        it('returns the total amount to be returned', async () => {
          // Withdraw winnings: ACCT3, ACCT4, ACCT5, ORACLE
          // ACCT3 winner
          let myWinningBets = bet3
          let myWinningVotes = vote3a
          let myLosingBets = toBN(0)
          let amounts = calculateNormalWinnings({
            myWinningBets,
            myWinningVotes,
            myLosingBets,
            escrowAmt,
            from: ACCT3,
            owner: OWNER,
            resultSetter: OWNER,
            ...calcParams,
          })
          let winningAmt = reduce(amounts, (res, val) => res.add(toBN(val)), toBN(0))
          sassert.bnEqual(await eventMethods.calculateWinnings(ACCT3).call(), winningAmt)

          // ACCT4 winner
          myWinningBets = bet4
          myWinningVotes = vote4a
          myLosingBets = toBN(0)
          amounts = calculateNormalWinnings({
            myWinningBets,
            myWinningVotes,
            myLosingBets,
            escrowAmt,
            from: ACCT4,
            owner: OWNER,
            resultSetter: OWNER,
            ...calcParams,
          })
          winningAmt = reduce(amounts, (res, val) => res.add(toBN(val)), toBN(0))
          sassert.bnEqual(await eventMethods.calculateWinnings(ACCT4).call(), winningAmt)

          // ACCT5 winner
          myWinningBets = toBN(0)
          myWinningVotes = vote5a
          myLosingBets = toBN(0)
          amounts = calculateNormalWinnings({
            myWinningBets,
            myWinningVotes,
            myLosingBets,
            escrowAmt,
            from: ACCT5,
            owner: OWNER,
            resultSetter: OWNER,
            ...calcParams,
          })
          winningAmt = reduce(amounts, (res, val) => res.add(toBN(val)), toBN(0))
          sassert.bnEqual(await eventMethods.calculateWinnings(ACCT5).call(), winningAmt)

          // CentralizedOracle winner
          myWinningBets = toBN(0)
          myWinningVotes = cOracleThreshold
          myLosingBets = toBN(0)
          amounts = calculateNormalWinnings({
            myWinningBets,
            myWinningVotes,
            myLosingBets,
            escrowAmt,
            from: OWNER,
            owner: OWNER,
            resultSetter: OWNER,
            ...calcParams,
          })
          winningAmt = reduce(amounts, (res, val) => res.add(toBN(val)), toBN(0))
          sassert.bnEqual(await eventMethods.calculateWinnings(OWNER).call(), winningAmt)

          // ACCT1 loser
          myWinningBets = toBN(0)
          myWinningVotes = toBN(0)
          myLosingBets = bet1
          amounts = calculateNormalWinnings({
            myWinningBets,
            myWinningVotes,
            myLosingBets,
            escrowAmt,
            from: ACCT1,
            owner: OWNER,
            resultSetter: OWNER,
            ...calcParams,
          })
          winningAmt = reduce(amounts, (res, val) => res.add(toBN(val)), toBN(0))
          sassert.bnEqual(await eventMethods.calculateWinnings(ACCT1).call(), winningAmt)

          // ACCT2 loser
          myWinningBets = toBN(0)
          myWinningVotes = toBN(0)
          myLosingBets = bet2
          amounts = calculateNormalWinnings({
            myWinningBets,
            myWinningVotes,
            myLosingBets,
            escrowAmt,
            from: ACCT2,
            owner: OWNER,
            resultSetter: OWNER,
            ...calcParams,
          })
          winningAmt = reduce(amounts, (res, val) => res.add(toBN(val)), toBN(0))
          sassert.bnEqual(await eventMethods.calculateWinnings(ACCT2).call(), winningAmt)
        })
      })

      describe('getWithdrawAmounts', () => {
        it('returns the amounts', async () => {
          // ACCT3 winner
          let playerWinningBets = bet3
          let playerWinningVotes = vote3a
          let playerLosingBets = toBN(0)
          let betRoundWinningAmt =
            calcParams.betRoundWinnersTotal
            .add(toBN(escrowAmt))
            .mul(playerWinningBets)
            .div(calcParams.betRoundWinnersTotal)
            .add(playerLosingBets)
          let voteRoundsWinningAmt =
            playerWinningVotes
            .mul(calcParams.voteRoundsLosersTotal)
            .div(calcParams.voteRoundsWinnersTotal)
            .add(playerWinningVotes)

          let voterReturn = voteRoundsWinningAmt
          let creatorReturn = toBN(0)
          let amts = await eventMethods.getWithdrawAmounts(ACCT3).call()
          sassert.bnEqual(amts[0], betRoundWinningAmt)
          sassert.bnEqual(amts[1], voterReturn)
          sassert.bnEqual(amts[2], creatorReturn)

          // ACCT4 winner
          playerWinningBets = bet4
          playerWinningVotes = vote4a
          playerLosingBets = toBN(0)
          betRoundWinningAmt =
            calcParams.betRoundWinnersTotal
            .add(toBN(escrowAmt))
            .mul(playerWinningBets)
            .div(calcParams.betRoundWinnersTotal)
            .add(playerLosingBets)
          voteRoundsWinningAmt =
            playerWinningVotes
            .mul(calcParams.voteRoundsLosersTotal)
            .div(calcParams.voteRoundsWinnersTotal)
            .add(playerWinningVotes)

          voterReturn = voteRoundsWinningAmt
          creatorReturn = toBN(0)
          amts = await eventMethods.getWithdrawAmounts(ACCT4).call()
          sassert.bnEqual(amts[0], betRoundWinningAmt)
          sassert.bnEqual(amts[1], voterReturn)
          sassert.bnEqual(amts[2], creatorReturn)

          // ACCT5 winner
          playerWinningBets = toBN(0)
          playerWinningVotes = vote5a
          betRoundWinningAmt =
            calcParams.betRoundWinnersTotal
            .add(toBN(escrowAmt))
            .mul(playerWinningBets)
            .div(calcParams.betRoundWinnersTotal)
            .add(playerLosingBets)
          voteRoundsWinningAmt =
            playerWinningVotes
            .mul(calcParams.voteRoundsLosersTotal)
            .div(calcParams.voteRoundsWinnersTotal)
            .add(playerWinningVotes)

          voterReturn = voteRoundsWinningAmt
          creatorReturn = toBN(0)
          amts = await eventMethods.getWithdrawAmounts(ACCT5).call()
          sassert.bnEqual(amts[0], betRoundWinningAmt)
          sassert.bnEqual(amts[1], voterReturn)
          sassert.bnEqual(amts[2], creatorReturn)
          
          // OWNER winner
          playerWinningBets = toBN(0)
          playerWinningVotes = cOracleThreshold
          betRoundWinningAmt =
            calcParams.betRoundWinnersTotal
            .add(toBN(escrowAmt))
            .mul(playerWinningBets)
            .div(calcParams.betRoundWinnersTotal)
            .add(playerLosingBets)
          voteRoundsWinningAmt =
            playerWinningVotes
            .mul(calcParams.voteRoundsLosersTotal)
            .div(calcParams.voteRoundsWinnersTotal)
            .add(playerWinningVotes)

          voterReturn = voteRoundsWinningAmt
          creatorReturn = toBN(0)
          amts = await eventMethods.getWithdrawAmounts(OWNER).call()
          sassert.bnEqual(amts[0], betRoundWinningAmt)
          sassert.bnEqual(amts[1], voterReturn)
          sassert.bnEqual(amts[2], creatorReturn)
        })
      })
    })

    describe('Invalid resultIndex scenario', () => {
      const cOracleResult = 0
      let bet1
      let bet2
      let bet3
      let bet4
      let cOracleThreshold
      let vote1a
      let vote2a

      beforeEach(async () => {
        let totalBets

        // Advance to betting time
        let currTime = await currentBlockTime()
        await timeMachine.increaseTime(betStartTime - currTime)
        assert.isAtLeast(await currentBlockTime(), betStartTime)
        assert.isBelow(await currentBlockTime(), betEndTime)

        // First round of betting
        bet1 = toSatoshi(12)
        await placeBet({
          nbotMethods,
          eventAddr,
          amtSatoshi: bet1.toString(),
          resultIndex: 2,
          from: ACCT1,
        })
        totalBets = bet1
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        bet2 = toSatoshi(23)
        await placeBet({
          nbotMethods,
          eventAddr,
          amtSatoshi: bet2.toString(),
          resultIndex: 1,
          from: ACCT2,
        })
        totalBets = totalBets.add(bet2)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        bet3 = toSatoshi(10)
        await placeBet({
          nbotMethods,
          eventAddr,
          amtSatoshi: bet3.toString(),
          resultIndex: 2,
          from: ACCT3,
        })
        totalBets = totalBets.add(bet3)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        bet4 = toSatoshi(5)
        await placeBet({
          nbotMethods,
          eventAddr,
          amtSatoshi: bet4.toString(),
          resultIndex: 2,
          from: ACCT4,
        })
        totalBets = totalBets.add(bet4)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        // Advance to result setting time
        currTime = await currentBlockTime()
        await timeMachine.increaseTime(resultSetStartTime - currTime)
        assert.isAtLeast(await currentBlockTime(), resultSetStartTime)

        // Set result 2
        cOracleThreshold =
          toBN(await eventMethods.currentConsensusThreshold().call())
        await setResult({
          nbotMethods,
          eventAddr,
          amt: cOracleThreshold.toString(),
          resultIndex: cOracleResult,
          from: OWNER,
        })
        totalBets = totalBets.add(cOracleThreshold)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)
        assert.equal(await eventMethods.currentResultIndex().call(), cOracleResult)
        assert.equal(await eventMethods.currentRound().call(), 1)

        // dOracle1 voting. Threshold does not hit and result stays 0.
        vote1a = toSatoshi(30)
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: vote1a.toString(),
          resultIndex: 2,
          from: ACCT1,
        })
        totalBets = totalBets.add(vote1a)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        vote2a = toSatoshi(30)
        await placeVote({
          nbotMethods,
          eventAddr,
          amtSatoshi: vote2a.toString(),
          resultIndex: 1,
          from: ACCT2,
        })
        totalBets = totalBets.add(vote2a)
        sassert.bnEqual(await eventMethods.totalBets().call(), totalBets)

        // Advance to arbitration end time
        currTime = await currentBlockTime()
        const arbEndTime = Number(await eventMethods.currentArbitrationEndTime().call())
        await timeMachine.increaseTime(arbEndTime - currTime)
        assert.isAtLeast(await currentBlockTime(), arbEndTime)
      })
      
      describe('calculateWinnings', () => {
        it('returns the players bet amount and calculates the vote winnings', async () => {
          // ACCT1 should get all their bets back
          sassert.bnEqual(await eventMethods.calculateWinnings(ACCT1).call(), bet1)

          // ACCT2 should get all their bets back
          sassert.bnEqual(await eventMethods.calculateWinnings(ACCT2).call(), bet2)

          // ACCT3 should get all their bets back
          sassert.bnEqual(await eventMethods.calculateWinnings(ACCT3).call(), bet3)

          // ACCT4 should get all their bets back
          sassert.bnEqual(await eventMethods.calculateWinnings(ACCT4).call(), bet4)
        })
      })

      describe('getWithdrawAmounts', () => {
        it('returns the amounts', async () => {
          const voteRoundsWinnersTotal = cOracleThreshold
          const voteRoundsLosersTotal = vote1a.add(vote2a)
          
          // ACCT1
          let playerWinningBets = toBN(0)
          let playerLosingBets = bet1
          let playerWinningVotes = toBN(0)
          let expected = calculateInvalidWinnings({
            playerWinningBets,
            playerLosingBets,
            playerWinningVotes,
            voteRoundsWinnersTotal,
            voteRoundsLosersTotal,
          })
          let amts = await eventMethods.getWithdrawAmounts(ACCT1).call()
          sassert.bnEqual(amts[0], expected.betRoundWinningAmt)
          sassert.bnEqual(amts[1], expected.voteRoundsWinningAmt)
          sassert.bnEqual(amts[2], toBN(0))

          // ACCT2
          playerWinningBets = toBN(0)
          playerLosingBets = bet2
          playerWinningVotes = toBN(0)
          expected = calculateInvalidWinnings({
            playerWinningBets,
            playerLosingBets,
            playerWinningVotes,
            voteRoundsWinnersTotal,
            voteRoundsLosersTotal,
          })
          amts = await eventMethods.getWithdrawAmounts(ACCT2).call()
          sassert.bnEqual(amts[0], expected.betRoundWinningAmt)
          sassert.bnEqual(amts[1], expected.voteRoundsWinningAmt)
          sassert.bnEqual(amts[2], toBN(0))

          // ACCT3
          playerWinningBets = toBN(0)
          playerLosingBets = bet3
          playerWinningVotes = toBN(0)
          expected = calculateInvalidWinnings({
            playerWinningBets,
            playerLosingBets,
            playerWinningVotes,
            voteRoundsWinnersTotal,
            voteRoundsLosersTotal,
          })
          amts = await eventMethods.getWithdrawAmounts(ACCT3).call()
          sassert.bnEqual(amts[0], expected.betRoundWinningAmt)
          sassert.bnEqual(amts[1], expected.voteRoundsWinningAmt)
          sassert.bnEqual(amts[2], toBN(0))

          // ACCT4
          playerWinningBets = toBN(0)
          playerLosingBets = bet4
          playerWinningVotes = toBN(0)
          expected = calculateInvalidWinnings({
            playerWinningBets,
            playerLosingBets,
            playerWinningVotes,
            voteRoundsWinnersTotal,
            voteRoundsLosersTotal,
          })
          amts = await eventMethods.getWithdrawAmounts(ACCT4).call()
          sassert.bnEqual(amts[0], expected.betRoundWinningAmt)
          sassert.bnEqual(amts[1], expected.voteRoundsWinningAmt)
          sassert.bnEqual(amts[2], toBN(0))

          // OWNER
          playerWinningBets = toBN(0)
          playerLosingBets = toBN(0)
          playerWinningVotes = cOracleThreshold
          expected = calculateInvalidWinnings({
            playerWinningBets,
            playerLosingBets,
            playerWinningVotes,
            voteRoundsWinnersTotal,
            voteRoundsLosersTotal,
          })
          amts = await eventMethods.getWithdrawAmounts(OWNER).call()
          sassert.bnEqual(amts[0], expected.betRoundWinningAmt)
          sassert.bnEqual(amts[1], expected.voteRoundsWinningAmt)
          sassert.bnEqual(amts[2], toBN(escrowAmt))
        })
      })
    })

    describe('default resultIndex scenario', () => {
      describe('calculateWinnings', () => {
        it('returns 0', async () => {
          assert.equal(
            await eventMethods.currentResultIndex().call(),
            DEFAULT_RESULT_INDEX
          )
          sassert.bnEqual(await eventMethods.calculateWinnings(OWNER).call(), 0)
        })
      })

      describe('getWithdrawAmounts', () => {
        it('returns array of 0s', async () => {
          assert.equal(
            await eventMethods.currentResultIndex().call(),
            DEFAULT_RESULT_INDEX
          )
          const amts = await eventMethods.getWithdrawAmounts(OWNER).call()
          sassert.bnEqual(amts[0], 0)
          sassert.bnEqual(amts[1], 0)
          sassert.bnEqual(amts[2], 0)
        })
      })
    })
  })
})
